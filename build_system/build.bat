@echo off
echo Building new version

set PYTHONPATH=%PYTHONPATH%;..\src\client;..\src\tools;..\src;
Python27\python build_client.py

echo Build completed!

:: ������ ������� ������ � ��������� ������
set /p build=<build.txt
set /p version=<version.txt
echo Current version = %version%.%build%
set /a build=%build%+1
echo %build% > build.txt
echo VERSION = %version%.%build% > ..\version.pri

set nameSuffix=v%version%.%build%
set buildDirectory=..\builds\NetworkGame_%nameSuffix%
echo %buildDirectory%
mkdir %buildDirectory%
rmdir /s/q dist\tcl
xcopy dist\main.exe %buildDirectory% /E
xcopy dist\freesansbold.ttf %buildDirectory% /E
mkdir %buildDirectory%\logs

rmdir /s/q dist

echo VERSION = %version%.%build%

git add .
if %errorlevel% NEQ 0 goto failed
git commit -m "Auto commit. New release %version%.%build%"
if %errorlevel% NEQ 0 goto failed
git tag %version%.%build%
if %errorlevel% NEQ 0 goto failed
git push origin master
if %errorlevel% NEQ 0 goto failed
git push origin --tags
if %errorlevel% NEQ 0 goto failed

echo Success!
exit 0
goto failed

:failed
echo Something goes wrong
exit 1
