#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging, sys
import client
import pygame
from pygame.locals import *
from threading import Thread
from time import sleep
from inputbox import *
from pygame import *
from player import *
from blocks import *
from monsters import *
from bullets import *
from indicator import *
from config import HEROES

logger = logging.getLogger((__name__))

logger.info('Starting NetworkGame Client')
logfile = 'logs/client.log'
loglevel = logging.DEBUG

logging.basicConfig(filename = logfile, format = '%(asctime)s [%(levelname)s] %(name)s: %(message)s', level = loglevel)

client = client.client()
pygame.init()

WIN_WIDTH = 1440
WIN_HEIGHT = 900
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)
BACKGROUND_COLOR = "#008800"

class pingThread(Thread):
    def __init__(self):
        Thread.__init__(self)
    def run(self):
        global client
        
        while True:
            if (client.hosting == True):
                client.waitPlayers()
            elif (client.connected == True):
                client.connected = client.ping()
            
            sleep(5)

class Camera(object):
    def __init__(self, camera_func, width, height):
        self.camera_func = camera_func
        self.state = Rect(0, 0, width, height)

    def apply(self, target):
        return target.rect.move(self.state.topleft)

    def update(self, target):
        self.state = self.camera_func(self.state, target.rect)
        
def camera_configure(camera, target_rect):
    l, t, _, _ = target_rect
    _, _, w, h = camera
    l, t = -l + WIN_WIDTH / 2, -t + WIN_HEIGHT / 2

    l = min(0, l)
    l = max(-(camera.width - WIN_WIDTH), l)
    t = max(-(camera.height - WIN_HEIGHT), t)
    t = min(0, t)

    return Rect(l, t, w, h)

def loadLevel(levelName):
    global playerX, playerY

    levelFile = open('../levels/%s.txt' % levelName)
    line = " "
    commands = []
    while line[0] != "/":
        line = levelFile.readline()
        if line[0] == "[":
            while line[0] != "]":
                line = levelFile.readline()
                if line[0] != "]":
                    endLine = line.find("|")
                    level.append(line[0: endLine])
                    
        if line[0] != "":
            commands = line.split()
            if len(commands) > 1:
                if commands[0] == "player":
                    playerX = int(commands[1])
                    playerY = int(commands[2])
                if commands[0] == "portal":
                    tp = BlockTeleport(int(commands[1]),int(commands[2]),int(commands[3]),int(commands[4]))
                    entities.add(tp)
                    platforms.append(tp)
                    animatedEntities.add(tp)
                if commands[0] == "monster":
                    mn = Monster(int(commands[1]),int(commands[2]),int(commands[3]),int(commands[4]),int(commands[5]),int(commands[6]))
                    entities.add(mn)
                    platforms.append(mn)
                    monsters.add(mn)

def main(funcName):
    
    if (client.hosting == True):
        client.sendMessage({'act' : 'startGame', 'game' : client.userName})
    
    loadLevel(client.selectedMap)
    pygame.init()
    screen = pygame.display.set_mode(DISPLAY)
    pygame.display.set_caption("Networkgame")
    
    bg = Surface((WIN_WIDTH, WIN_HEIGHT))
    bg.fill(Color(BACKGROUND_COLOR))
    
    left = right = False
    up = False
    running = False
    down = False
     
    maxHp = len(HEROES[client.selectedHero - 1].get('hp'))
    maxSpeed = len(HEROES[client.selectedHero - 1].get('speed'))
    maxPower = len(HEROES[client.selectedHero - 1].get('power'))
    
    hero = Player(playerX, playerY, maxHp, maxSpeed, maxPower, client.selectedHero)
    entities.add(hero)
    #hero1 = Player(playerX, playerY + 210, maxHp, maxSpeed, maxPower, client.selectedHero + 2)
    #entities.add(hero1)
           
    timer = pygame.time.Clock()
    x = y = 0
    for row in level:
        for col in row:
            if col == "-":
                pf = Platform(x, y)
                entities.add(pf)
                platforms.append(pf)
            if col == "*":
                bd = BlockDie(x, y)
                entities.add(bd)
                platforms.append(bd)
            if col == "t":
                tb = Tablet(x, y)
                entities.add(tb)
                platforms.append(tb)
                animatedEntities.add(tb)
            if col == "p":
                tb = MovingBlock(x, y, 5, 0)
                entities.add(tb)
                platforms.append(tb)
                movePlatforms.add(tb)
   
            x += PLATFORM_WIDTH
        y += PLATFORM_HEIGHT
        x = 0
    
    total_level_width  = len(level[0]) * PLATFORM_WIDTH
    total_level_height = len(level) * PLATFORM_HEIGHT
    
    camera = Camera(camera_configure, total_level_width, total_level_height)
    HP_PIC = ('../textures/indicators/hp.png')
    indicatorHp = Indicator(20, 0, 40, HP_PIC)
    indicators.add(indicatorHp)
    
    LASER_PIC = ('../textures/indicators/laser.png')
    indicatorAmmo = Indicator(0, 0, 80, LASER_PIC)
    indicators.add(indicatorAmmo)
    
    GUN_PIC = ('../textures/indicators/machine_gun.png')
    indicatorAmmo = Indicator(0, 0, 120, GUN_PIC)
    indicators.add(indicatorAmmo)
    
    PISTOL_PIC = ('../textures/indicators/pistol.png')
    indicatorAmmo = Indicator(0, 0, 160, PISTOL_PIC)
    indicators.add(indicatorAmmo)
    
    
    bulletShift = (0, 0)
    
    while not hero.winner:
        timer.tick(60)
        for e in pygame.event.get():
            if e.type == QUIT:
                client.sendMessage({'act' : 'disconnect'})                
                sys.exit()
            if (e.type == KEYDOWN):
                if (e.key == K_ESCAPE):
                    client.sendMessage({'act' : 'keydown', 'key' : 'esc'})                    
                    mainMenu(funcName)
                if (e.key == K_UP or e.key == K_w):
                    client.sendMessage({'act' : 'keydown', 'key' : 'up'})
                    up = True
                if (e.key == K_LEFT or e.key == K_a):
                    client.sendMessage({'act' : 'keydown', 'key' : 'left'})
                    left = True
                    hero.lastDest = 0
                if (e.key == K_RIGHT or e.key == K_d):
                    client.sendMessage({'act' : 'keydown', 'key' : 'right'})
                    right = True
                    hero.lastDest = 1
                if (e.key == K_LSHIFT):
                    client.sendMessage({'act' : 'keydown', 'key' : 'shift'})
                    running = True
                #if (e.key == K_s):
                #    client.sendMessage({'act' : 'keydown', 'key' : 'down'})
                #    down = True
                if (e.key == K_SPACE):
                    camera_w = 400
                    if (hero.rect.centerx - camera_w > 0 and hero.rect.centerx + camera_w < 1000):
                        tmp = hero.rect.centerx - camera_w
                        hero_x = hero.rect.centerx - tmp
                    elif (hero.rect.centerx - camera_w > 0 and hero.rect.centerx + camera_w > 1000):
                        hero_x = hero.rect.centerx - 300
                    else:
                        hero_x = hero.rect.centerx          
                    
                    if (hero.lastDest == 1):
                        if (down):
                            bulletShift = (40, 65)
                        else:
                            bulletShift = (40, 55)
                        speed = 1
                    else:
                        if (down): 
                            bulletShift = (0, 65)
                        else:
                            bulletShift = (0, 55)                        
                        speed = -1
                    
                    x = int(hero.rect.x + bulletShift[0])
                    y = int(hero.rect.y + bulletShift[1])
                    
                    if (hero.weapon == 1 and hero.laser_ammo > 0):
                        hero.laser_ammo -= 1
                        mn = Bullet(x, y, speed * 10, hero)
                        client.sendMessage({'act' : 'keyup', 'key' : 'space', 'ammo' : 1})
                    elif (hero.weapon == 2 and hero.gun_ammo > 0):
                        hero.gun_ammo -= 1
                        mn = Bullet(x, y + 5, speed * 15, hero)
                        client.sendMessage({'act' : 'keyup', 'key' : 'space', 'ammo' : 2})
                    elif (hero.weapon == 3 and hero.pistol_ammo > 0):
                        hero.pistol_ammo -= 1
                        mn = KittenBullet(x, y - 5, speed * 5, hero)
                        client.sendMessage({'act' : 'keyup', 'key' : 'space', 'ammo' : 3})
                        
                    if (mn != None):
                        entities.add(mn)
                        platforms.append(mn)
                        bullets.add(mn)

            if (e.type == KEYUP):
                if (e.key == K_UP or e.key == K_w):
                    client.sendMessage({'act' : 'keyup', 'key' : 'up'})
                    up = False
                if (e.key == K_RIGHT or e.key == K_d):
                    client.sendMessage({'act' : 'keyup', 'key' : 'right'})
                    right = False
                if (e.key == K_LEFT or e.key == K_a):
                    client.sendMessage({'act' : 'keyup', 'key' : 'left'})
                    left = False
                if (e.key == K_LSHIFT):
                    client.sendMessage({'act' : 'keyup', 'key' : 'shift'})
                    running = False
                #if (e.key == K_s):
                #    client.sendMessage({'act' : 'keyup', 'key' : 'down'})
                #    down = False
                if (e.key == K_r):
                    if (hero.weapon == 1):
                        hero.laser_ammo = 50
                    elif (hero.weapon == 2):
                        hero.gun_ammo = 50
                    else:
                        hero.pistol_ammo = 50
                if (e.key == K_1):
                    client.sendMessage({'act' : 'keyup', 'key' : '1'})
                    hero.changeWeapon(1)
                if (e.key == K_2):
                    client.sendMessage({'act' : 'keyup', 'key' : '2'})
                    hero.changeWeapon(2)
                if (e.key == K_3):
                    client.sendMessage({'act' : 'keyup', 'key' : '3'})
                    hero.changeWeapon(3)

        screen.blit(bg, (0, 0))

        bullets.update(platforms)
        movePlatforms.update(platforms)
        animatedEntities.update()
        
        monsters.update(platforms)
        
        camera.update(hero)
        indicators.update(camera.state.topleft[0], camera.state.topleft[1])
        
        hero.update(left, right, up, running, down, platforms)
        
        for e in entities:
            screen.blit(e.image, camera.apply(e))
        
        for ind in indicators:
            screen.blit(ind.image, camera.apply(ind))
        
        font = pygame.font.Font("freesansbold.ttf", 25)
        #font = pygame.font.Font(None, 25)
        
        if (client.connected):
            conn = font.render("Connected", True, (0, 128, 0))
        else:                
            conn = font.render("Not connected", True, (255, 0, 0))
            
        userName = font.render(client.userName, True, (0, 0, 255))
        
        hp = font.render(str(hero.hp), True, (128, 0, 0))
        laser_ammo = font.render(str(hero.laser_ammo), True, (128, 0, 0))
        gun_ammo = font.render(str(hero.gun_ammo), True, (128, 0, 0))
        pistol_ammo = font.render(str(hero.pistol_ammo), True, (128, 0, 0))
        
        screen.blit(userName, [0, 20])
        screen.blit(conn, [0, 0])
        screen.blit(hp, [80, 48])
        screen.blit(laser_ammo, [80, 88])
        screen.blit(gun_ammo, [80, 128])
        screen.blit(pistol_ammo, [80, 160])
        
        scoreTxt = font.render("Score: ", True, (0, 0, 255))
        score = font.render(str(hero.score), True, (0, 0, 255))
        
        screen.blit(scoreTxt, [600, 40])
        screen.blit(score, [660, 40])
        
        pygame.display.update()
        
level = []
entities = pygame.sprite.Group()
animatedEntities = pygame.sprite.Group()
monsters = pygame.sprite.Group()
bullets = pygame.sprite.Group()
indicators = pygame.sprite.Group()
platforms = []
movePlatforms = pygame.sprite.Group()

WHITE = (255, 255, 255)
RED = (255, 0, 0)
BLACK = (0, 0, 0)
 
class MenuItem(pygame.font.Font):
    def __init__(self, text, font = "freesansbold.ttf", font_size = 30,
                 font_color = WHITE, (pos_x, pos_y) = (0, 0)):
 
        pygame.font.Font.__init__(self, font, font_size)
        self.text = text
        self.font_size = font_size
        self.font_color = font_color
        self.label = self.render(self.text, 1, self.font_color)
        self.width = self.label.get_rect().width
        self.height = self.label.get_rect().height
        self.dimensions = (self.width, self.height)
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.position = pos_x, pos_y
 
    def is_mouse_selection(self, (posx, posy)):
        if (posx >= self.pos_x and posx <= self.pos_x + self.width) and \
            (posy >= self.pos_y and posy <= self.pos_y + self.height):
                return True
        return False
 
    def set_position(self, x, y):
        self.position = (x, y)
        self.pos_x = x
        self.pos_y = y
 
    def set_font_color(self, rgb_tuple):
        self.font_color = rgb_tuple
        self.label = self.render(self.text, 1, self.font_color)
 
class GameMenu():
    def __init__(self, screen, items, funcs, bg_color = BLACK, font = "freesansbold.ttf", font_size = 30,
                 font_color = WHITE, msg = '', positions = None):
        self.screen = screen
        self.scr_width = self.screen.get_rect().width
        self.scr_height = self.screen.get_rect().height
        
        self.msg = msg
 
        self.bg_color = bg_color
        self.clock = pygame.time.Clock()
 
        self.funcs = funcs
        self.items = []
        for index, item in enumerate(items):
            menu_item = MenuItem(item, font, font_size, font_color)
 
            if (positions == None):
                t_h = len(items) * menu_item.height
                pos_x = (self.scr_width / 2) - (menu_item.width / 2)
                pos_y = (self.scr_height / 2) - (t_h / 2) + ((index * 2) + index * menu_item.height)
            else:
                pos_x = positions[index][0]
                pos_y = positions[index][1]
            
            menu_item.set_position(pos_x, pos_y)
            self.items.append(menu_item)
 
        self.mouse_is_visible = True
        self.cur_item = None
 
    def set_mouse_visibility(self):
        if self.mouse_is_visible:
            pygame.mouse.set_visible(True)
        else:
            pygame.mouse.set_visible(False)
 
    def set_keyboard_selection(self, key):
        for item in self.items:
            item.set_italic(False)
            item.set_font_color(WHITE)
 
        if self.cur_item is None:
            self.cur_item = 0
        else:
            if key == pygame.K_UP and \
                    self.cur_item > 0:
                self.cur_item -= 1
            elif key == pygame.K_UP and \
                    self.cur_item == 0:
                self.cur_item = len(self.items) - 1
            elif key == pygame.K_DOWN and \
                    self.cur_item < len(self.items) - 1:
                self.cur_item += 1
            elif key == pygame.K_DOWN and \
                    self.cur_item == len(self.items) - 1:
                self.cur_item = 0
 
        self.items[self.cur_item].set_italic(True)
        self.items[self.cur_item].set_font_color(RED)
 
        if key == pygame.K_SPACE or key == pygame.K_RETURN:
            text = self.items[self.cur_item].text
            self.funcs[text]()
 
    def set_mouse_selection(self, item, mpos):
        if item.is_mouse_selection(mpos):
            item.set_font_color(RED)
            item.set_italic(True)
        else:
            item.set_font_color(WHITE)
            item.set_italic(False)
 
    def run(self, mode = None):
        mainloop = True
        while mainloop:
            self.clock.tick(50)
 
            mpos = pygame.mouse.get_pos()
 
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    mainloop = False
                    raise SystemExit, "QUIT"
                if event.type == pygame.KEYDOWN:
                    self.mouse_is_visible = False
                    self.set_keyboard_selection(event.key)
                if event.type == pygame.MOUSEBUTTONDOWN:
                    for item in self.items:
                        if item.is_mouse_selection(mpos):
                            #print item.text
                            if (item.text == 'Quit'):
                                self.funcs[item.text]()
                            else:
                                self.funcs[item.text](item.text)
 
            if pygame.mouse.get_rel() != (0, 0):
                self.mouse_is_visible = True
                self.cur_item = None
 
            self.set_mouse_visibility()
 
            self.screen.fill(self.bg_color)
 
            for item in self.items:
                if self.mouse_is_visible:
                    self.set_mouse_selection(item, mpos)
                self.screen.blit(item.label, item.position)
 
            font = pygame.font.Font("freesansbold.ttf", 25)
            #font = pygame.font.Font(None, 25)
            
            if (client.connected):
                conn = font.render("Connected", True, (0, 128, 0))
            else:                
                conn = font.render("Not connected", True, (255, 0, 0))
            
            msg = font.render(self.msg, True, (255, 255, 255))
            userName = font.render(client.userName, True, (0, 0, 255))
            
            self.screen.blit(msg, [220, 200])
            self.screen.blit(userName, [0, 20])
            self.screen.blit(conn, [0, 0])
            
            if (mode == 1 or mode == 2):
                sMap = font.render("Select map:", True, (255, 255, 255))
                map = font.render(str(client.selectedMap), True, (255, 255, 255))
                self.screen.blit(sMap, [355, 85])
                self.screen.blit(map, [415, 115])
                
                sHero = font.render("Select hero:", True, (255, 255, 255))
                hero = font.render(str(client.selectedHero), True, (255, 255, 255))
                self.screen.blit(sHero, [755, 85])
                self.screen.blit(hero, [815, 115])
                
                hp = font.render("Hp:", True, (255, 255, 255))
                heroHp = font.render(HEROES[client.selectedHero - 1].get('hp'), True, (255, 255, 255))
                self.screen.blit(hp, [755, 155])
                self.screen.blit(heroHp, [800, 155])
                
                speed = font.render("Speed:", True, (255, 255, 255))
                heroSpeed = font.render(HEROES[client.selectedHero - 1].get('speed'), True, (255, 255, 255))
                self.screen.blit(speed, [710, 205])
                self.screen.blit(heroSpeed, [800, 205])
                
                power = font.render("Power:", True, (255, 255, 255))
                heroPower = font.render(HEROES[client.selectedHero - 1].get('power'), True, (255, 255, 255))
                self.screen.blit(power, [710, 255])
                self.screen.blit(heroPower, [800, 255])
                
            if (mode == 2 and client.connected):
                x = 500
                y = 85
                
                plrs = font.render('Players:', True, (0, 255, 0))
                self.screen.blit(plrs, [x, y])
                
                y += 30
                plr = font.render(client.userName, True, (255, 255, 255))
                self.screen.blit(plr, [x, y])
                
                if (client.players != None):
                    for player in client.players:
                        y += 30
                        plr = font.render(player, True, (255, 255, 255))
                        self.screen.blit(plr, [x, y])
                        
            if (mode == 3):                
                games = font.render('Games:', True, (0, 255, 0))
                self.screen.blit(games, [650, 85])
 
            pygame.display.flip()

#screen = pygame.display.set_mode((640, 480), 0, 32)
screen = pygame.display.set_mode(DISPLAY, 0, 32)
mapsCount = 2
heroesCount = len(HEROES)

def start(funcName):

    if (client.hosting == True):
        client.hosting = False
        client.players = []
        client.sendMessage({'act' : 'newGameF'})

    funcs = {'Solo game': soloGame,
             'Create game': createGame,
             'Find game': findGame,
             'Back': mainMenu}
    
    keys = ['Solo game', 'Create game', 'Find game', 'Back']
    
    gm = GameMenu(screen, keys, funcs)
    gm.run()

def findGame(funcName):    
    funcs = {'Back' : start}
    keys = ['Back']
    positions = [[700, 800]]
    
    games = client.findGames()
    games = games.split(',')
    
    x = 255
    y = 85
    
    for game in games:
        if (len(game) > 0 and client.userName != game):
            y += 30
            funcs.update({game : joinGame})
            keys.append(game)
            positions.append([x, y])
    
    
    gm = GameMenu(screen, keys, funcs, positions = positions)
    gm.run(mode = 3)

def joinGame(game):
    
    if (client.joinGame(game)):
        funcs = {'Back' : findGame}
        keys = ['Back']
        
        for i in range (0, 10):
            if (client.waitForStart(game) == False):
                sleep(2)
            else:
                break
        
        
    findGame(game)

def soloGame(funcName):

    funcs = {'<' : prevMap,
             '>' : nextMap,
             '<<' : prevHero,
             '>>' : nextHero,
             'Back' : start,
             'Next' : main}
    
    keys = ['<', '>', '<<', '>>', 'Back', 'Next']
    
    positions = [[390, 110], [440, 110], [770, 110], [840, 110], [600, 800], [800, 800]]
    
    gm = GameMenu(screen, keys, funcs, positions = positions)
    gm.run(mode = 1)

def createGame(funcName):
        
    funcs = {'<' : prevMap,
             '>' : nextMap,
             '<<' : prevHero,
             '>>' : nextHero,
             'Back' : start,
             'Next' : main}
    
    keys = ['<', '>', '<<', '>>', 'Back', 'Next']
    
    positions = [[390, 110], [440, 110], [770, 110], [840, 110], [600, 800], [800, 800]]
    
    gm = GameMenu(screen, keys, funcs, positions = positions)
    
    if (client.hosting == False):
        client.sendMessage({'act' : 'newGameT'})
        client.hosting = True
    
    gm.run(mode = 2)          

def prevHero(funcName):
    if (client.selectedHero == 1):
        client.selectedHero = heroesCount
    else:
        client.selectedHero -= 1
        
    createGame(funcName)
    
def nextHero(funcName):
    if (client.selectedHero == heroesCount):
        client.selectedHero = 1
    else:
        client.selectedHero += 1
        
    createGame(funcName)
    
def prevMap(funcName):
    if (client.selectedMap == 1):
        client.selectedMap = mapsCount
    else:
        client.selectedMap -= 1
        
    createGame(funcName)
    

def nextMap(funcName):
    if (client.selectedMap == mapsCount):
        client.selectedMap = 1
    else:
        client.selectedMap += 1
        
    createGame(funcName)

def waitPlayers(funcName):
    pass
 
def enterIp(funcName):
    serversFile = open('../servers/servers.txt')
    
    funcs = {}
    keys = []
    positions = []
    y = 100
    x = 650
    
    line = "1"
    while True:
        line = serversFile.readline()
        if (len(line) > 0):
            line = line[0:-1]
            funcs.update({line : selectServer})
            keys.append(line)
            positions.append([x - 30, y])
            y += 30
        else:
            break
    
    serversFile.close()
    
    funcs.update({'New' : newIp, 'Back': options})
    keys.append('New')
    keys.append('Back')
    positions.append([650, 700])
    positions.append([650, 800])
    
    gm = GameMenu(screen, keys, funcs, positions = positions)
    gm.run()
    
def selectServer(serverIP):
    msg = client.connect(serverIP)
    
    funcs = {'Back' : enterIp}
    keys = ['Back']
    gm = GameMenu(screen, funcs.keys(), funcs, msg = msg)
    gm.run()
    
    enterIp(serverIP)

def newIp(funcName):
    serverIP = ask(screen, "Enter server IP")
    
    funcs = {'Back' : enterIp}
    keys = ['Back']
    
    if serverIP:
        serversFile = open('tools/servers/servers.txt', 'a')
        serversFile.write(serverIP + "\n")
        serversFile.close()
        msg = client.connect(serverIP)
        gm = GameMenu(screen, funcs.keys(), funcs, msg = msg)
        gm.run()
    
    enterIp(funcName)
    
def enterName(funcName):

    funcs = {'Back': options
            }
    
    userName = ask(screen, "Enter your name")
    if userName:
        msg = client.changeName(userName)
        gm = GameMenu(screen, funcs.keys(), funcs, msg = msg)
        gm.run()
        
    options(funcName)

def options(funcName):
    
    funcs = {'Server IP': enterIp,
             'Player Name': enterName,
             'Back': mainMenu}
    
    keys = ['Server IP', 'Player Name', 'Back']
    
    gm = GameMenu(screen, keys, funcs)
    gm.run()

def mainMenu(funcName):
    global level, entities, animatedEntities, monsters, bullets, indicators, platforms
    
    level = []
    entities = pygame.sprite.Group()
    animatedEntities = pygame.sprite.Group()
    monsters = pygame.sprite.Group()
    bullets = pygame.sprite.Group()
    indicators = pygame.sprite.Group()
    platforms = []
    
    thrd = pingThread()
    thrd.setDaemon(True)
    thrd.start()

    funcs = {'Start': start,
             'Options': options,
             'Quit': sys.exit}
 
    pygame.display.set_caption('Networkgame')
    gm = GameMenu(screen, funcs.keys(), funcs)
    gm.run()


if __name__ == "__main__":
    mainMenu('mainMenu')