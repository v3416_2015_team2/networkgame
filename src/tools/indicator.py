# -*- coding: utf-8 -*-

from pygame import *
from config import INDICATOR_WIDTH, INDICATOR_HEIGHT, INDICATOR_COLOR
import pyganim

class Indicator(sprite.Sprite):
    def __init__(self, startX, startY, shift, PIC):
        sprite.Sprite.__init__(self)
        self.image = Surface((INDICATOR_WIDTH, INDICATOR_HEIGHT))
        self.image.fill(Color(INDICATOR_COLOR))
        self.rect = Rect(startX, startY, INDICATOR_WIDTH, INDICATOR_HEIGHT)
        self.image.set_colorkey(Color(INDICATOR_COLOR))
        self.startX = startX
        self.startY = startY
        self.shift = shift
        boltAnim = []
        boltAnim.append((PIC, 0.3))
        self.boltAnim = pyganim.PygAnimation(boltAnim)
        self.boltAnim.play()
         
    def update(self, x, y):
           
        self.image.fill(Color(INDICATOR_COLOR))
        self.boltAnim.blit(self.image, (0, 0))
            
        self.rect.y = abs(y) + self.shift
        self.rect.x = abs(x)
        
        
        
