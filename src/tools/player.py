#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pygame import *
import pyganim
import blocks
import monsters
import bullets
from config import *
from time import time, sleep

class Player(sprite.Sprite):
    def __init__(self, x, y, hp, speed, power, heroType):
        sprite.Sprite.__init__(self)
        self.xvel = 0
        self.startX = x
        self.startY = y
        self.yvel = 0
        self.onGround = False
        self.weapon = 1
        self.hp = hp * 10    
        self.maxHp = hp * 10
        self.speed = speed
        self.jump_power = power * 2
        self.laser_ammo = 50
        self.gun_ammo = 50
        self.pistol_ammo = 50
        self.score = 0
        self.heroType = heroType - 1
        self.lastDest = 1
        self.lastBlockDmg = None
        self.image = Surface((PLAYER_WIDTH, PLAYER_HEIGHT))
        self.image.fill(Color(PLAYER_COLOR))
        self.rect = Rect(x + 10, y, PLAYER_WIDTH - 10, PLAYER_HEIGHT)
        self.small_sprite = sprite.Sprite()
        self.small_sprite.rect = Rect(x + 10, y, PLAYER_WIDTH - 10, PLAYER_HEIGHT - 20)
        self.image.set_colorkey(Color(PLAYER_COLOR))

        boltAnim = []
        boltAnimSuperSpeed = []
        for anim in HERO_ANIM[self.heroType]['ANIMATION_RIGHT_LASER']:
            boltAnim.append((anim, ANIMATION_DELAY))
            boltAnimSuperSpeed.append((anim, ANIMATION_SUPER_SPEED_DELAY))
        self.boltAnimRight = pyganim.PygAnimation(boltAnim)
        self.boltAnimRight.play()
        
        self.boltAnimRightSuperSpeed = pyganim.PygAnimation(boltAnimSuperSpeed)
        self.boltAnimRightSuperSpeed.play()
     
        boltAnim = []
        boltAnimSuperSpeed = [] 
        for anim in HERO_ANIM[self.heroType]['ANIMATION_LEFT_LASER']:
            boltAnim.append((anim, ANIMATION_DELAY))
            boltAnimSuperSpeed.append((anim, ANIMATION_SUPER_SPEED_DELAY))
        self.boltAnimLeft = pyganim.PygAnimation(boltAnim)
        self.boltAnimLeft.play()
        
        self.boltAnimLeftSuperSpeed = pyganim.PygAnimation(boltAnimSuperSpeed)
        self.boltAnimLeftSuperSpeed.play()
        
        self.boltAnimStayRight = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_STAY_R_LASER'])
        self.boltAnimStayRight.play()
        self.boltAnimStayRight.blit(self.image, (0, 0))
        
        self.boltAnimStayLeft = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_STAY_L_LASER'])
        self.boltAnimStayLeft.play()
        
        boltAnim = []
        for anim in HERO_ANIM[self.heroType]['ANIMATION_DOWN_LEFT_LASER']:
            boltAnim.append((anim, ANIMATION_DELAY))
        self.boltAnimDownLeft = pyganim.PygAnimation(boltAnim)
        self.boltAnimDownLeft.play()
        
        boltAnim = []
        for anim in HERO_ANIM[self.heroType]['ANIMATION_DOWN_RIGHT_LASER']:
            boltAnim.append((anim, ANIMATION_DELAY))
        self.boltAnimDownRight = pyganim.PygAnimation(boltAnim)
        self.boltAnimDownRight.play()
        
        self.boltAnimJumpLeft = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_JUMP_LEFT_LASER'])
        self.boltAnimJumpLeft.play()
        
        self.boltAnimJumpRight = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_JUMP_RIGHT_LASER'])
        self.boltAnimJumpRight.play()
        
        self.winner = False
        

    def update(self, left, right, up, running, down, platforms):
                
        if (self.lastBlockDmg == None or time() - self.lastBlockDmg > 1):
            if up:
                if self.onGround:
                    self.yvel = -self.jump_power
                    if running and (left or right):
                            self.yvel -= JUMP_EXTRA_POWER
                    self.image.fill(Color(PLAYER_COLOR))
                    if (self.lastDest == 1):
                        self.boltAnimJumpRight.blit(self.image, (0, 0))
                    else:                        
                        self.boltAnimJumpLeft.blit(self.image, (0, 0))
                           
            if left:
                self.xvel = -self.speed
                self.image.fill(Color(PLAYER_COLOR))
                if running:
                    self.xvel -= MOVE_EXTRA_SPEED
                    if not up:
                        self.boltAnimLeftSuperSpeed.blit(self.image, (0, 0))
                else:
                    if not up:
                        self.boltAnimLeft.blit(self.image, (0, 0))
                if up:
                        self.boltAnimJumpLeft.blit(self.image, (0, 0))
     
            if right:
                self.xvel = self.speed
                self.image.fill(Color(PLAYER_COLOR))
                if running:
                    self.xvel += MOVE_EXTRA_SPEED
                    if not up:
                        self.boltAnimRightSuperSpeed.blit(self.image, (0, 0))
                else:
                    if not up:
                        self.boltAnimRight.blit(self.image, (0, 0)) 
                if up:
                        self.boltAnimJumpRight.blit(self.image, (0, 0))
             
            if not(left or right):
                self.xvel = 0
                if not up:
                    self.image.fill(Color(PLAYER_COLOR))
                    if (self.lastDest == 1):
                        if down:
                            self.boltAnimDownRight.blit(self.image, (0, 0))
                        else:
                            self.boltAnimStayRight.blit(self.image, (0, 0))
                    else:                 
                        if down:
                            self.boltAnimDownLeft.blit(self.image, (0, 0))
                        else:       
                            self.boltAnimStayLeft.blit(self.image, (0, 0))
                
        if not self.onGround:
            self.yvel +=  GRAVITY
        
        self.onGround = False
        self.rect.y += self.yvel
        self.small_sprite.rect.y += self.yvel
        self.collide(0, self.yvel, platforms)

        self.rect.x += self.xvel
        self.small_sprite.rect.x += self.xvel
        
        self.collide(self.xvel, 0, platforms)        
   
    def collide(self, xvel, yvel, platforms):
        for p in platforms:
            if sprite.collide_rect(self, p):
                if isinstance(p, blocks.BlockDie) or isinstance(p, monsters.Monster):
                    
                    if (self.lastBlockDmg != None and time() - self.lastBlockDmg < 1):
                        if (xvel > 0):
                            self.rect.right = p.rect.left
                            self.xvel = -2
                        elif (xvel < 0):
                            self.rect.left = p.rect.right
                            self.xvel = 2
                        
                        if (yvel > 0):
                            self.yvel = -10
                        elif (yvel < 0):
                            self.yvel = 10
                        return
                    
                    self.hp -= 5
                    self.lastBlockDmg = time()
                    if (self.hp <= 0):
                        self.die()
                    else:
                        if (xvel > 0):
                            self.rect.right = p.rect.left
                            self.xvel = -2
                        elif (xvel < 0):
                            self.rect.left = p.rect.right
                            self.xvel = 2
                            
                        if (yvel > 0):
                            self.yvel = -10
                        elif (yvel < 0):
                            self.yvel = 10
                elif isinstance(p, bullets.Bullets):
                    pass
                elif isinstance(p, blocks.Tablet):
                    if (p.active == False):
                        pass
                    else:
                        if (self.hp + 50 <= self.maxHp):
                            self.hp += 50
                        else:
                            self.hp = self.maxHp
                        p.active = False
                        p.lastUse = time()
                        p.rect.x = 0
                        p.rect.y = 0
                elif isinstance(p, blocks.BlockTeleport):
                    self.teleporting(p.goX, p.goY)
                else:
                    if xvel > 0:
                        self.rect.right = p.rect.left

                    if xvel < 0:
                        self.rect.left = p.rect.right

                    if yvel > 0:
                        self.rect.bottom = p.rect.top
                        self.onGround = True
                        self.yvel = 0
                        
                        if (isinstance(p, blocks.MovingBlock)):
                            self.xvel += p.xvel

                    if yvel < 0:
                        self.rect.top = p.rect.bottom
                        self.yvel = 0
    
    def changeWeapon(self, weapon):
        self.weapon = weapon
        
        if (self.weapon == 1):
            boltAnim = []
            boltAnimSuperSpeed = []
            for anim in HERO_ANIM[self.heroType]['ANIMATION_RIGHT_LASER']:
                boltAnim.append((anim, ANIMATION_DELAY))
                boltAnimSuperSpeed.append((anim, ANIMATION_SUPER_SPEED_DELAY))
            self.boltAnimRight = pyganim.PygAnimation(boltAnim)
            self.boltAnimRight.play()
            
            self.boltAnimRightSuperSpeed = pyganim.PygAnimation(boltAnimSuperSpeed)
            self.boltAnimRightSuperSpeed.play()
      
            boltAnim = []
            boltAnimSuperSpeed = [] 
            for anim in HERO_ANIM[self.heroType]['ANIMATION_LEFT_LASER']:
                boltAnim.append((anim, ANIMATION_DELAY))
                boltAnimSuperSpeed.append((anim, ANIMATION_SUPER_SPEED_DELAY))
            self.boltAnimLeft = pyganim.PygAnimation(boltAnim)
            self.boltAnimLeft.play()
            
            self.boltAnimLeftSuperSpeed = pyganim.PygAnimation(boltAnimSuperSpeed)
            self.boltAnimLeftSuperSpeed.play()
            
            self.boltAnimStayRight = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_STAY_R_LASER'])
            self.boltAnimStayRight.play()
            
            self.boltAnimStayLeft = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_STAY_L_LASER'])
            self.boltAnimStayLeft.play()
            
            boltAnim = []
            for anim in HERO_ANIM[self.heroType]['ANIMATION_DOWN_LEFT_LASER']:
                boltAnim.append((anim, ANIMATION_DELAY))
            self.boltAnimDownLeft = pyganim.PygAnimation(boltAnim)
            self.boltAnimDownLeft.play()
            
            boltAnim = []
            for anim in HERO_ANIM[self.heroType]['ANIMATION_DOWN_RIGHT_LASER']:
                boltAnim.append((anim, ANIMATION_DELAY))
            self.boltAnimDownRight = pyganim.PygAnimation(boltAnim)
            self.boltAnimDownRight.play()
            
            self.boltAnimJumpLeft = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_JUMP_LEFT_LASER'])
            self.boltAnimJumpLeft.play()
            
            self.boltAnimJumpRight = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_JUMP_RIGHT_LASER'])
            self.boltAnimJumpRight.play()
            
        elif (self.weapon == 2):
            boltAnim = []
            boltAnimSuperSpeed = []
            for anim in HERO_ANIM[self.heroType]['ANIMATION_RIGHT_GUN']:
                boltAnim.append((anim, ANIMATION_DELAY))
                boltAnimSuperSpeed.append((anim, ANIMATION_SUPER_SPEED_DELAY))
            self.boltAnimRight = pyganim.PygAnimation(boltAnim)
            self.boltAnimRight.play()
            
            self.boltAnimRightSuperSpeed = pyganim.PygAnimation(boltAnimSuperSpeed)
            self.boltAnimRightSuperSpeed.play()
   
            boltAnim = []
            boltAnimSuperSpeed = [] 
            for anim in HERO_ANIM[self.heroType]['ANIMATION_LEFT_GUN']:
                boltAnim.append((anim, ANIMATION_DELAY))
                boltAnimSuperSpeed.append((anim, ANIMATION_SUPER_SPEED_DELAY))
            self.boltAnimLeft = pyganim.PygAnimation(boltAnim)
            self.boltAnimLeft.play()
            
            self.boltAnimLeftSuperSpeed = pyganim.PygAnimation(boltAnimSuperSpeed)
            self.boltAnimLeftSuperSpeed.play()
            
            boltAnim = []
            for anim in HERO_ANIM[self.heroType]['ANIMATION_DOWN_LEFT_GUN']:
                boltAnim.append((anim, ANIMATION_DELAY))
            self.boltAnimDownLeft = pyganim.PygAnimation(boltAnim)
            self.boltAnimDownLeft.play()
            
            boltAnim = []
            for anim in HERO_ANIM[self.heroType]['ANIMATION_DOWN_RIGHT_GUN']:
                boltAnim.append((anim, ANIMATION_DELAY))
            self.boltAnimDownRight = pyganim.PygAnimation(boltAnim)
            self.boltAnimDownRight.play()
            
            self.boltAnimStayRight = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_STAY_R_GUN'])
            self.boltAnimStayRight.play()
            
            self.boltAnimStayLeft = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_STAY_L_GUN'])
            self.boltAnimStayLeft.play()
            
            self.boltAnimJumpLeft = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_JUMP_LEFT_GUN'])
            self.boltAnimJumpLeft.play()
            
            self.boltAnimJumpRight = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_JUMP_RIGHT_GUN'])
            self.boltAnimJumpRight.play()
        elif (self.weapon == 3):
            boltAnim = []
            boltAnimSuperSpeed = []
            for anim in HERO_ANIM[self.heroType]['ANIMATION_RIGHT_PISTOL']:
                boltAnim.append((anim, ANIMATION_DELAY))
                boltAnimSuperSpeed.append((anim, ANIMATION_SUPER_SPEED_DELAY))
            self.boltAnimRight = pyganim.PygAnimation(boltAnim)
            self.boltAnimRight.play()
            
            self.boltAnimRightSuperSpeed = pyganim.PygAnimation(boltAnimSuperSpeed)
            self.boltAnimRightSuperSpeed.play()
     
            boltAnim = []
            boltAnimSuperSpeed = [] 
            for anim in HERO_ANIM[self.heroType]['ANIMATION_LEFT_PISTOL']:
                boltAnim.append((anim, ANIMATION_DELAY))
                boltAnimSuperSpeed.append((anim, ANIMATION_SUPER_SPEED_DELAY))
            self.boltAnimLeft = pyganim.PygAnimation(boltAnim)
            self.boltAnimLeft.play()
            
            self.boltAnimLeftSuperSpeed = pyganim.PygAnimation(boltAnimSuperSpeed)
            self.boltAnimLeftSuperSpeed.play()
            
            boltAnim = []
            for anim in HERO_ANIM[self.heroType]['ANIMATION_DOWN_LEFT_PISTOL']:
                boltAnim.append((anim, ANIMATION_DELAY))
            self.boltAnimDownLeft = pyganim.PygAnimation(boltAnim)
            self.boltAnimDownLeft.play()
            
            boltAnim = []
            for anim in HERO_ANIM[self.heroType]['ANIMATION_DOWN_RIGHT_PISTOL']:
                boltAnim.append((anim, ANIMATION_DELAY))
            self.boltAnimDownRight = pyganim.PygAnimation(boltAnim)
            self.boltAnimDownRight.play()
            
            self.boltAnimStayRight = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_STAY_R_PISTOL'])
            self.boltAnimStayRight.play()
            
            self.boltAnimStayLeft = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_STAY_L_PISTOL'])
            self.boltAnimStayLeft.play()
            
            self.boltAnimJumpLeft = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_JUMP_LEFT_PISTOL'])
            self.boltAnimJumpLeft.play()
            
            self.boltAnimJumpRight = pyganim.PygAnimation(HERO_ANIM[self.heroType]['ANIMATION_JUMP_RIGHT_PISTOL'])
            self.boltAnimJumpRight.play()

    def teleporting(self, goX, goY):
        self.rect.x = goX
        self.rect.y = goY
        
    def die(self):
        sleep(5)
        self.hp = self.maxHp
        self.teleporting(self.startX, self.startY)
