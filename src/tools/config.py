#!/usr/bin/env python
# -*- coding: utf-8 -*-

BG_COLOR = "#008800"

HEROES = [{'hp' : '----------', 'speed' : '-----', 'power' : '-------'},
          {'hp' : '------', 'speed' : '----------', 'power' : '------'},
          {'hp' : '-------------', 'speed' : '---', 'power' : '------'},
          {'hp' : '--------', 'speed' : '--------', 'power' : '--------'}]

PLATFORM_WIDTH = 32
PLATFORM_HEIGHT = 32
PLATFORM_COLOR = BG_COLOR
ANIMATION_BLOCKTELEPORT = [('../textures/blocks/portal2.png'),
                           ('../textures/blocks/portal1.png')]   
PLATFORM_PIC = "../textures/blocks/platform.png"
DIE_BLOCK_PIC = "../textures/blocks/dieBlock2.png"
TABLET_PIC = "../textures/blocks/tablet.png"

KITTEN_WIDTH = 57
KITTEN_HEIGHT = 40
BULLET_WIDTH = 30
BULLET_HEIGHT = 15
BULLET_COLOR = BG_COLOR
KITTEN_PIC = ('../textures/weapons/pistol_kitten_1.png')
BULLET_PIC_L = ('../textures/weapons/bullet_l.png')
BULLET_PIC_R = ('../textures/weapons/bullet_r.png')
BOOM_PIC = ('../textures/weapons/boom.png')

INDICATOR_WIDTH = 80
INDICATOR_HEIGHT = 35
INDICATOR_COLOR = BG_COLOR

MOVE_SPEED = 7
MOVE_EXTRA_SPEED = 2.5
PLAYER_WIDTH = 100
PLAYER_HEIGHT = 120
WIDTH_d = 63
HEIGHT_d = 87
PLAYER_COLOR =  BG_COLOR
JUMP_POWER = 15
JUMP_EXTRA_POWER = 1
GRAVITY = 0.35
ANIMATION_DELAY = 0.1
ANIMATION_SUPER_SPEED_DELAY = 0.05

HERO_ANIM = []
ANIMATION_RIGHT_LASER = [('../textures/hero/blue/laser/r_1.png'),
                   ('../textures/hero/blue/laser/r_2.png'),
                   ('../textures/hero/blue/laser/r_3.png'),
                   ('../textures/hero/blue/laser/r_4.png'),
                   ('../textures/hero/blue/laser/r_5.png'),
                   ('../textures/hero/blue/laser/r_6.png'),
                   ('../textures/hero/blue/laser/r_7.png'),
                   ('../textures/hero/blue/laser/r_8.png'),
                   ('../textures/hero/blue/laser/r_9.png'),
                   ('../textures/hero/blue/laser/r_10.png'),
                   ('../textures/hero/blue/laser/r_11.png')]

ANIMATION_RIGHT_GUN = [('../textures/hero/blue/gun/r_1.png'),
                   ('../textures/hero/blue/gun/r_2.png'),
                   ('../textures/hero/blue/gun/r_3.png'),
                   ('../textures/hero/blue/gun/r_4.png'),
                   ('../textures/hero/blue/gun/r_5.png'),
                   ('../textures/hero/blue/gun/r_6.png'),
                   ('../textures/hero/blue/gun/r_7.png'),
                   ('../textures/hero/blue/gun/r_8.png'),
                   ('../textures/hero/blue/gun/r_9.png'),
                   ('../textures/hero/blue/gun/r_10.png'),
                   ('../textures/hero/blue/gun/r_11.png')]

ANIMATION_RIGHT_PISTOL = [('../textures/hero/blue/pistol/r_1.png'),
                   ('../textures/hero/blue/pistol/r_2.png'),
                   ('../textures/hero/blue/pistol/r_3.png'),
                   ('../textures/hero/blue/pistol/r_4.png'),
                   ('../textures/hero/blue/pistol/r_5.png'),
                   ('../textures/hero/blue/pistol/r_6.png'),
                   ('../textures/hero/blue/pistol/r_7.png'),
                   ('../textures/hero/blue/pistol/r_8.png'),
                   ('../textures/hero/blue/pistol/r_9.png'),
                   ('../textures/hero/blue/pistol/r_10.png'),
                   ('../textures/hero/blue/pistol/r_11.png')]

ANIMATION_LEFT_LASER = [('../textures/hero/blue/laser/l_1.png'),
                  ('../textures/hero/blue/laser/l_2.png'),
                  ('../textures/hero/blue/laser/l_3.png'),
                  ('../textures/hero/blue/laser/l_4.png'),
                  ('../textures/hero/blue/laser/l_5.png'),
                  ('../textures/hero/blue/laser/l_6.png'),
                  ('../textures/hero/blue/laser/l_7.png'),
                  ('../textures/hero/blue/laser/l_8.png'),
                  ('../textures/hero/blue/laser/l_9.png'),
                  ('../textures/hero/blue/laser/l_10.png'),
                  ('../textures/hero/blue/laser/l_11.png')]

ANIMATION_LEFT_GUN = [('../textures/hero/blue/gun/l_1.png'),
                  ('../textures/hero/blue/gun/l_2.png'),
                  ('../textures/hero/blue/gun/l_3.png'),
                  ('../textures/hero/blue/gun/l_4.png'),
                  ('../textures/hero/blue/gun/l_5.png'),
                  ('../textures/hero/blue/gun/l_6.png'),
                  ('../textures/hero/blue/gun/l_7.png'),
                  ('../textures/hero/blue/gun/l_8.png'),
                  ('../textures/hero/blue/gun/l_9.png'),
                  ('../textures/hero/blue/gun/l_10.png'),
                  ('../textures/hero/blue/gun/l_11.png')]

ANIMATION_LEFT_PISTOL = [('../textures/hero/blue/pistol/l_1.png'),
                  ('../textures/hero/blue/pistol/l_2.png'),
                  ('../textures/hero/blue/pistol/l_3.png'),
                  ('../textures/hero/blue/pistol/l_4.png'),
                  ('../textures/hero/blue/pistol/l_5.png'),
                  ('../textures/hero/blue/pistol/l_6.png'),
                  ('../textures/hero/blue/pistol/l_7.png'),
                  ('../textures/hero/blue/pistol/l_8.png'),
                  ('../textures/hero/blue/pistol/l_9.png'),
                  ('../textures/hero/blue/pistol/l_10.png'),
                  ('../textures/hero/blue/pistol/l_11.png')]

ANIMATION_DOWN_LEFT_LASER = [('../textures/hero/blue/laser/dl_1.png'),
                             ('../textures/hero/blue/laser/dl_1.png')]

ANIMATION_DOWN_RIGHT_LASER = [('../textures/hero/blue/laser/dr_1.png'),
                              ('../textures/hero/blue/laser/dr_1.png')]

ANIMATION_DOWN_LEFT_GUN = [('../textures/hero/blue/gun/dl_1.png'),
                           ('../textures/hero/blue/gun/dl_1.png')]

ANIMATION_DOWN_RIGHT_GUN = [('../textures/hero/blue/gun/dr_1.png'),
                            ('../textures/hero/blue/gun/dr_1.png')]

ANIMATION_DOWN_LEFT_PISTOL = [('../textures/hero/blue/pistol/dl_1.png'),
                             ('../textures/hero/blue/pistol/dl_1.png')]

ANIMATION_DOWN_RIGHT_PISTOL = [('../textures/hero/blue/pistol/dr_1.png'),
                            ('../textures/hero/blue/pistol/dr_1.png')]

ANIMATION_JUMP_LEFT_LASER = [('../textures/hero/blue/laser/jl_1.png', 0.1)]
ANIMATION_JUMP_RIGHT_LASER = [('../textures/hero/blue/laser/jr_1.png', 0.1)]

ANIMATION_JUMP_LEFT_GUN = [('../textures/hero/blue/gun/jl_1.png', 0.1)]
ANIMATION_JUMP_RIGHT_GUN = [('../textures/hero/blue/gun/jr_1.png', 0.1)]

ANIMATION_JUMP_LEFT_PISTOL = [('../textures/hero/blue/pistol/jl_1.png', 0.1)]
ANIMATION_JUMP_RIGHT_PISTOL = [('../textures/hero/blue/pistol/jr_1.png', 0.1)]

ANIMATION_STAY_R_LASER = [('../textures/hero/blue/laser/sr_1.png', 0.1)]
ANIMATION_STAY_L_LASER = [('../textures/hero/blue/laser/sl_1.png', 0.1)]
ANIMATION_STAY_R_GUN = [('../textures/hero/blue/gun/sr_1.png', 0.1)]
ANIMATION_STAY_L_GUN = [('../textures/hero/blue/gun/sl_1.png', 0.1)]
ANIMATION_STAY_R_PISTOL = [('../textures/hero/blue/pistol/sr_1.png', 0.1)]
ANIMATION_STAY_L_PISTOL = [('../textures/hero/blue/pistol/sl_1.png', 0.1)]

HERO_ANIM.append({'ANIMATION_RIGHT_LASER' : ANIMATION_RIGHT_LASER, 'ANIMATION_RIGHT_GUN' : ANIMATION_RIGHT_GUN, 
                  'ANIMATION_LEFT_LASER' : ANIMATION_LEFT_LASER, 'ANIMATION_LEFT_GUN' : ANIMATION_LEFT_GUN, 
                  'ANIMATION_DOWN_LEFT_LASER' : ANIMATION_DOWN_LEFT_LASER, 'ANIMATION_DOWN_RIGHT_LASER' : ANIMATION_DOWN_RIGHT_LASER,
                  'ANIMATION_DOWN_LEFT_GUN' : ANIMATION_DOWN_LEFT_GUN, 'ANIMATION_DOWN_RIGHT_GUN' : ANIMATION_DOWN_RIGHT_GUN, 
                  'ANIMATION_JUMP_LEFT_LASER' : ANIMATION_JUMP_LEFT_LASER, 'ANIMATION_JUMP_RIGHT_LASER' : ANIMATION_JUMP_RIGHT_LASER, 
                  'ANIMATION_JUMP_LEFT_GUN' : ANIMATION_JUMP_LEFT_GUN, 'ANIMATION_JUMP_RIGHT_GUN' : ANIMATION_JUMP_RIGHT_GUN,
                  'ANIMATION_STAY_R_LASER' : ANIMATION_STAY_R_LASER, 'ANIMATION_STAY_L_LASER' : ANIMATION_STAY_L_LASER, 
                  'ANIMATION_STAY_R_GUN' : ANIMATION_STAY_R_GUN, 'ANIMATION_STAY_L_GUN' : ANIMATION_STAY_L_GUN,
                  'ANIMATION_RIGHT_PISTOL' : ANIMATION_RIGHT_PISTOL, 'ANIMATION_LEFT_PISTOL' : ANIMATION_LEFT_PISTOL,
                  'ANIMATION_DOWN_LEFT_PISTOL' : ANIMATION_DOWN_LEFT_PISTOL, 'ANIMATION_DOWN_RIGHT_PISTOL' : ANIMATION_DOWN_RIGHT_PISTOL,
                  'ANIMATION_JUMP_LEFT_PISTOL' : ANIMATION_JUMP_LEFT_PISTOL, 'ANIMATION_JUMP_RIGHT_PISTOL' : ANIMATION_JUMP_RIGHT_PISTOL,
                  'ANIMATION_STAY_R_PISTOL' : ANIMATION_STAY_R_PISTOL, 'ANIMATION_STAY_L_PISTOL' : ANIMATION_STAY_L_PISTOL})

ANIMATION_RIGHT_LASER = [('../textures/hero/black/laser/r_1.png'),
                       ('../textures/hero/black/laser/r_2.png'),
                       ('../textures/hero/black/laser/r_3.png'),
                       ('../textures/hero/black/laser/r_4.png'),
                       ('../textures/hero/black/laser/r_5.png'),
                       ('../textures/hero/black/laser/r_6.png'),
                       ('../textures/hero/black/laser/r_7.png'),
                       ('../textures/hero/black/laser/r_8.png'),
                       ('../textures/hero/black/laser/r_9.png'),
                       ('../textures/hero/black/laser/r_10.png'),
                       ('../textures/hero/black/laser/r_11.png')]

ANIMATION_RIGHT_GUN = [('../textures/hero/black/gun/r_1.png'),
                       ('../textures/hero/black/gun/r_2.png'),
                       ('../textures/hero/black/gun/r_3.png'),
                       ('../textures/hero/black/gun/r_4.png'),
                       ('../textures/hero/black/gun/r_5.png'),
                       ('../textures/hero/black/gun/r_6.png'),
                       ('../textures/hero/black/gun/r_7.png'),
                       ('../textures/hero/black/gun/r_8.png'),
                       ('../textures/hero/black/gun/r_9.png'),
                       ('../textures/hero/black/gun/r_10.png'),
                       ('../textures/hero/black/gun/r_11.png')]

ANIMATION_RIGHT_PISTOL = [('../textures/hero/black/pistol/r_1.png'),
                           ('../textures/hero/black/pistol/r_2.png'),
                           ('../textures/hero/black/pistol/r_3.png'),
                           ('../textures/hero/black/pistol/r_4.png'),
                           ('../textures/hero/black/pistol/r_5.png'),
                           ('../textures/hero/black/pistol/r_6.png'),
                           ('../textures/hero/black/pistol/r_7.png'),
                           ('../textures/hero/black/pistol/r_8.png'),
                           ('../textures/hero/black/pistol/r_9.png'),
                           ('../textures/hero/black/pistol/r_10.png'),
                           ('../textures/hero/black/pistol/r_11.png')]

ANIMATION_LEFT_LASER = [('../textures/hero/black/laser/l_1.png'),
                      ('../textures/hero/black/laser/l_2.png'),
                      ('../textures/hero/black/laser/l_3.png'),
                      ('../textures/hero/black/laser/l_4.png'),
                      ('../textures/hero/black/laser/l_5.png'),
                      ('../textures/hero/black/laser/l_6.png'),
                      ('../textures/hero/black/laser/l_7.png'),
                      ('../textures/hero/black/laser/l_8.png'),
                      ('../textures/hero/black/laser/l_9.png'),
                      ('../textures/hero/black/laser/l_10.png'),
                      ('../textures/hero/black/laser/l_11.png')]

ANIMATION_LEFT_GUN = [('../textures/hero/black/gun/l_1.png'),
                      ('../textures/hero/black/gun/l_2.png'),
                      ('../textures/hero/black/gun/l_3.png'),
                      ('../textures/hero/black/gun/l_4.png'),
                      ('../textures/hero/black/gun/l_5.png'),
                      ('../textures/hero/black/gun/l_6.png'),
                      ('../textures/hero/black/gun/l_7.png'),
                      ('../textures/hero/black/gun/l_8.png'),
                      ('../textures/hero/black/gun/l_9.png'),
                      ('../textures/hero/black/gun/l_10.png'),
                      ('../textures/hero/black/gun/l_11.png')]

ANIMATION_LEFT_PISTOL = [('../textures/hero/black/pistol/l_1.png'),
                      ('../textures/hero/black/pistol/l_2.png'),
                      ('../textures/hero/black/pistol/l_3.png'),
                      ('../textures/hero/black/pistol/l_4.png'),
                      ('../textures/hero/black/pistol/l_5.png'),
                      ('../textures/hero/black/pistol/l_6.png'),
                      ('../textures/hero/black/pistol/l_7.png'),
                      ('../textures/hero/black/pistol/l_8.png'),
                      ('../textures/hero/black/pistol/l_9.png'),
                      ('../textures/hero/black/pistol/l_10.png'),
                      ('../textures/hero/black/pistol/l_11.png')]

ANIMATION_DOWN_LEFT_LASER = [('../textures/hero/black/laser/dl_1.png'),
                             ('../textures/hero/black/laser/dl_1.png')]

ANIMATION_DOWN_RIGHT_LASER = [('../textures/hero/black/laser/dr_1.png'),
                              ('../textures/hero/black/laser/dr_1.png')]

ANIMATION_DOWN_LEFT_GUN = [('../textures/hero/black/gun/dl_1.png'),
                           ('../textures/hero/black/gun/dl_1.png')]

ANIMATION_DOWN_RIGHT_GUN = [('../textures/hero/black/gun/dr_1.png'),
                            ('../textures/hero/black/gun/dr_1.png')]

ANIMATION_DOWN_LEFT_PISTOL = [('../textures/hero/black/pistol/dl_1.png'),
                             ('../textures/hero/black/pistol/dl_1.png')]

ANIMATION_DOWN_RIGHT_PISTOL = [('../textures/hero/black/pistol/dr_1.png'),
                            ('../textures/hero/black/pistol/dr_1.png')]

ANIMATION_JUMP_LEFT_LASER = [('../textures/hero/black/laser/jl_1.png', 0.1)]
ANIMATION_JUMP_RIGHT_LASER = [('../textures/hero/black/laser/jr_1.png', 0.1)]

ANIMATION_JUMP_LEFT_GUN = [('../textures/hero/black/gun/jl_1.png', 0.1)]
ANIMATION_JUMP_RIGHT_GUN = [('../textures/hero/black/gun/jr_1.png', 0.1)]

ANIMATION_JUMP_LEFT_PISTOL = [('../textures/hero/black/pistol/jl_1.png', 0.1)]
ANIMATION_JUMP_RIGHT_PISTOL = [('../textures/hero/black/pistol/jr_1.png', 0.1)]

ANIMATION_STAY_R_LASER = [('../textures/hero/black/laser/sr_1.png', 0.1)]
ANIMATION_STAY_L_LASER = [('../textures/hero/black/laser/sl_1.png', 0.1)]
ANIMATION_STAY_R_GUN = [('../textures/hero/black/gun/sr_1.png', 0.1)]
ANIMATION_STAY_L_GUN = [('../textures/hero/black/gun/sl_1.png', 0.1)]
ANIMATION_STAY_R_PISTOL = [('../textures/hero/black/pistol/sr_1.png', 0.1)]
ANIMATION_STAY_L_PISTOL = [('../textures/hero/black/pistol/sl_1.png', 0.1)]

HERO_ANIM.append({'ANIMATION_RIGHT_LASER' : ANIMATION_RIGHT_LASER, 'ANIMATION_RIGHT_GUN' : ANIMATION_RIGHT_GUN, 
                  'ANIMATION_LEFT_LASER' : ANIMATION_LEFT_LASER, 'ANIMATION_LEFT_GUN' : ANIMATION_LEFT_GUN, 
                  'ANIMATION_DOWN_LEFT_LASER' : ANIMATION_DOWN_LEFT_LASER, 'ANIMATION_DOWN_RIGHT_LASER' : ANIMATION_DOWN_RIGHT_LASER,
                  'ANIMATION_DOWN_LEFT_GUN' : ANIMATION_DOWN_LEFT_GUN, 'ANIMATION_DOWN_RIGHT_GUN' : ANIMATION_DOWN_RIGHT_GUN, 
                  'ANIMATION_JUMP_LEFT_LASER' : ANIMATION_JUMP_LEFT_LASER, 'ANIMATION_JUMP_RIGHT_LASER' : ANIMATION_JUMP_RIGHT_LASER, 
                  'ANIMATION_JUMP_LEFT_GUN' : ANIMATION_JUMP_LEFT_GUN, 'ANIMATION_JUMP_RIGHT_GUN' : ANIMATION_JUMP_RIGHT_GUN,
                  'ANIMATION_STAY_R_LASER' : ANIMATION_STAY_R_LASER, 'ANIMATION_STAY_L_LASER' : ANIMATION_STAY_L_LASER, 
                  'ANIMATION_STAY_R_GUN' : ANIMATION_STAY_R_GUN, 'ANIMATION_STAY_L_GUN' : ANIMATION_STAY_L_GUN,
                  'ANIMATION_RIGHT_PISTOL' : ANIMATION_RIGHT_PISTOL, 'ANIMATION_LEFT_PISTOL' : ANIMATION_LEFT_PISTOL,
                  'ANIMATION_DOWN_LEFT_PISTOL' : ANIMATION_DOWN_LEFT_PISTOL, 'ANIMATION_DOWN_RIGHT_PISTOL' : ANIMATION_DOWN_RIGHT_PISTOL,
                  'ANIMATION_JUMP_LEFT_PISTOL' : ANIMATION_JUMP_LEFT_PISTOL, 'ANIMATION_JUMP_RIGHT_PISTOL' : ANIMATION_JUMP_RIGHT_PISTOL,
                  'ANIMATION_STAY_R_PISTOL' : ANIMATION_STAY_R_PISTOL, 'ANIMATION_STAY_L_PISTOL' : ANIMATION_STAY_L_PISTOL})

ANIMATION_RIGHT_LASER = [('../textures/hero/brown/laser/r_1.png'),
                       ('../textures/hero/brown/laser/r_2.png'),
                       ('../textures/hero/brown/laser/r_3.png'),
                       ('../textures/hero/brown/laser/r_4.png'),
                       ('../textures/hero/brown/laser/r_5.png'),
                       ('../textures/hero/brown/laser/r_6.png'),
                       ('../textures/hero/brown/laser/r_7.png'),
                       ('../textures/hero/brown/laser/r_8.png'),
                       ('../textures/hero/brown/laser/r_9.png'),
                       ('../textures/hero/brown/laser/r_10.png'),
                       ('../textures/hero/brown/laser/r_11.png')]

ANIMATION_RIGHT_GUN = [('../textures/hero/brown/gun/r_1.png'),
                       ('../textures/hero/brown/gun/r_2.png'),
                       ('../textures/hero/brown/gun/r_3.png'),
                       ('../textures/hero/brown/gun/r_4.png'),
                       ('../textures/hero/brown/gun/r_5.png'),
                       ('../textures/hero/brown/gun/r_6.png'),
                       ('../textures/hero/brown/gun/r_7.png'),
                       ('../textures/hero/brown/gun/r_8.png'),
                       ('../textures/hero/brown/gun/r_9.png'),
                       ('../textures/hero/brown/gun/r_10.png'),
                       ('../textures/hero/brown/gun/r_11.png')]

ANIMATION_RIGHT_PISTOL = [('../textures/hero/brown/pistol/r_1.png'),
                           ('../textures/hero/brown/pistol/r_2.png'),
                           ('../textures/hero/brown/pistol/r_3.png'),
                           ('../textures/hero/brown/pistol/r_4.png'),
                           ('../textures/hero/brown/pistol/r_5.png'),
                           ('../textures/hero/brown/pistol/r_6.png'),
                           ('../textures/hero/brown/pistol/r_7.png'),
                           ('../textures/hero/brown/pistol/r_8.png'),
                           ('../textures/hero/brown/pistol/r_9.png'),
                           ('../textures/hero/brown/pistol/r_10.png'),
                           ('../textures/hero/brown/pistol/r_11.png')]

ANIMATION_LEFT_LASER = [('../textures/hero/brown/laser/l_1.png'),
                      ('../textures/hero/brown/laser/l_2.png'),
                      ('../textures/hero/brown/laser/l_3.png'),
                      ('../textures/hero/brown/laser/l_4.png'),
                      ('../textures/hero/brown/laser/l_5.png'),
                      ('../textures/hero/brown/laser/l_6.png'),
                      ('../textures/hero/brown/laser/l_7.png'),
                      ('../textures/hero/brown/laser/l_8.png'),
                      ('../textures/hero/brown/laser/l_9.png'),
                      ('../textures/hero/brown/laser/l_10.png'),
                      ('../textures/hero/brown/laser/l_11.png')]

ANIMATION_LEFT_GUN = [('../textures/hero/brown/gun/l_1.png'),
                      ('../textures/hero/brown/gun/l_2.png'),
                      ('../textures/hero/brown/gun/l_3.png'),
                      ('../textures/hero/brown/gun/l_4.png'),
                      ('../textures/hero/brown/gun/l_5.png'),
                      ('../textures/hero/brown/gun/l_6.png'),
                      ('../textures/hero/brown/gun/l_7.png'),
                      ('../textures/hero/brown/gun/l_8.png'),
                      ('../textures/hero/brown/gun/l_9.png'),
                      ('../textures/hero/brown/gun/l_10.png'),
                      ('../textures/hero/brown/gun/l_11.png')]

ANIMATION_LEFT_PISTOL = [('../textures/hero/brown/pistol/l_1.png'),
                      ('../textures/hero/brown/pistol/l_2.png'),
                      ('../textures/hero/brown/pistol/l_3.png'),
                      ('../textures/hero/brown/pistol/l_4.png'),
                      ('../textures/hero/brown/pistol/l_5.png'),
                      ('../textures/hero/brown/pistol/l_6.png'),
                      ('../textures/hero/brown/pistol/l_7.png'),
                      ('../textures/hero/brown/pistol/l_8.png'),
                      ('../textures/hero/brown/pistol/l_9.png'),
                      ('../textures/hero/brown/pistol/l_10.png'),
                      ('../textures/hero/brown/pistol/l_11.png')]

ANIMATION_DOWN_LEFT_LASER = [('../textures/hero/brown/laser/dl_1.png'),
                             ('../textures/hero/brown/laser/dl_1.png')]

ANIMATION_DOWN_RIGHT_LASER = [('../textures/hero/brown/laser/dr_1.png'),
                              ('../textures/hero/brown/laser/dr_1.png')]

ANIMATION_DOWN_LEFT_GUN = [('../textures/hero/brown/gun/dl_1.png'),
                           ('../textures/hero/brown/gun/dl_1.png')]

ANIMATION_DOWN_RIGHT_GUN = [('../textures/hero/brown/gun/dr_1.png'),
                            ('../textures/hero/brown/gun/dr_1.png')]

ANIMATION_DOWN_LEFT_PISTOL = [('../textures/hero/brown/pistol/dl_1.png'),
                             ('../textures/hero/brown/pistol/dl_1.png')]

ANIMATION_DOWN_RIGHT_PISTOL = [('../textures/hero/brown/pistol/dr_1.png'),
                               ('../textures/hero/brown/pistol/dr_1.png')]

ANIMATION_JUMP_LEFT_LASER = [('../textures/hero/brown/laser/jl_1.png', 0.1)]
ANIMATION_JUMP_RIGHT_LASER = [('../textures/hero/brown/laser/jr_1.png', 0.1)]

ANIMATION_JUMP_LEFT_GUN = [('../textures/hero/brown/gun/jl_1.png', 0.1)]
ANIMATION_JUMP_RIGHT_GUN = [('../textures/hero/brown/gun/jr_1.png', 0.1)]

ANIMATION_JUMP_LEFT_PISTOL = [('../textures/hero/brown/pistol/jl_1.png', 0.1)]
ANIMATION_JUMP_RIGHT_PISTOL = [('../textures/hero/brown/pistol/jr_1.png', 0.1)]

ANIMATION_STAY_R_LASER = [('../textures/hero/brown/laser/sr_1.png', 0.1)]
ANIMATION_STAY_L_LASER = [('../textures/hero/brown/laser/sl_1.png', 0.1)]
ANIMATION_STAY_R_GUN = [('../textures/hero/brown/gun/sr_1.png', 0.1)]
ANIMATION_STAY_L_GUN = [('../textures/hero/brown/gun/sl_1.png', 0.1)]
ANIMATION_STAY_R_PISTOL = [('../textures/hero/brown/pistol/sr_1.png', 0.1)]
ANIMATION_STAY_L_PISTOL = [('../textures/hero/brown/pistol/sl_1.png', 0.1)]

HERO_ANIM.append({'ANIMATION_RIGHT_LASER' : ANIMATION_RIGHT_LASER, 'ANIMATION_RIGHT_GUN' : ANIMATION_RIGHT_GUN, 
                  'ANIMATION_LEFT_LASER' : ANIMATION_LEFT_LASER, 'ANIMATION_LEFT_GUN' : ANIMATION_LEFT_GUN, 
                  'ANIMATION_DOWN_LEFT_LASER' : ANIMATION_DOWN_LEFT_LASER, 'ANIMATION_DOWN_RIGHT_LASER' : ANIMATION_DOWN_RIGHT_LASER,
                  'ANIMATION_DOWN_LEFT_GUN' : ANIMATION_DOWN_LEFT_GUN, 'ANIMATION_DOWN_RIGHT_GUN' : ANIMATION_DOWN_RIGHT_GUN, 
                  'ANIMATION_JUMP_LEFT_LASER' : ANIMATION_JUMP_LEFT_LASER, 'ANIMATION_JUMP_RIGHT_LASER' : ANIMATION_JUMP_RIGHT_LASER, 
                  'ANIMATION_JUMP_LEFT_GUN' : ANIMATION_JUMP_LEFT_GUN, 'ANIMATION_JUMP_RIGHT_GUN' : ANIMATION_JUMP_RIGHT_GUN,
                  'ANIMATION_STAY_R_LASER' : ANIMATION_STAY_R_LASER, 'ANIMATION_STAY_L_LASER' : ANIMATION_STAY_L_LASER, 
                  'ANIMATION_STAY_R_GUN' : ANIMATION_STAY_R_GUN, 'ANIMATION_STAY_L_GUN' : ANIMATION_STAY_L_GUN,
                  'ANIMATION_RIGHT_PISTOL' : ANIMATION_RIGHT_PISTOL, 'ANIMATION_LEFT_PISTOL' : ANIMATION_LEFT_PISTOL,
                  'ANIMATION_DOWN_LEFT_PISTOL' : ANIMATION_DOWN_LEFT_PISTOL, 'ANIMATION_DOWN_RIGHT_PISTOL' : ANIMATION_DOWN_RIGHT_PISTOL,
                  'ANIMATION_JUMP_LEFT_PISTOL' : ANIMATION_JUMP_LEFT_PISTOL, 'ANIMATION_JUMP_RIGHT_PISTOL' : ANIMATION_JUMP_RIGHT_PISTOL,
                  'ANIMATION_STAY_R_PISTOL' : ANIMATION_STAY_R_PISTOL, 'ANIMATION_STAY_L_PISTOL' : ANIMATION_STAY_L_PISTOL})

ANIMATION_RIGHT_LASER = [('../textures/hero/dark_brown/laser/r_1.png'),
                       ('../textures/hero/dark_brown/laser/r_2.png'),
                       ('../textures/hero/dark_brown/laser/r_3.png'),
                       ('../textures/hero/dark_brown/laser/r_4.png'),
                       ('../textures/hero/dark_brown/laser/r_5.png'),
                       ('../textures/hero/dark_brown/laser/r_6.png'),
                       ('../textures/hero/dark_brown/laser/r_7.png'),
                       ('../textures/hero/dark_brown/laser/r_8.png'),
                       ('../textures/hero/dark_brown/laser/r_9.png'),
                       ('../textures/hero/dark_brown/laser/r_10.png'),
                       ('../textures/hero/dark_brown/laser/r_11.png')]

ANIMATION_RIGHT_GUN = [('../textures/hero/dark_brown/gun/r_1.png'),
                       ('../textures/hero/dark_brown/gun/r_2.png'),
                       ('../textures/hero/dark_brown/gun/r_3.png'),
                       ('../textures/hero/dark_brown/gun/r_4.png'),
                       ('../textures/hero/dark_brown/gun/r_5.png'),
                       ('../textures/hero/dark_brown/gun/r_6.png'),
                       ('../textures/hero/dark_brown/gun/r_7.png'),
                       ('../textures/hero/dark_brown/gun/r_8.png'),
                       ('../textures/hero/dark_brown/gun/r_9.png'),
                       ('../textures/hero/dark_brown/gun/r_10.png'),
                       ('../textures/hero/dark_brown/gun/r_11.png')]

ANIMATION_RIGHT_PISTOL = [('../textures/hero/dark_brown/pistol/r_1.png'),
                           ('../textures/hero/dark_brown/pistol/r_2.png'),
                           ('../textures/hero/dark_brown/pistol/r_3.png'),
                           ('../textures/hero/dark_brown/pistol/r_4.png'),
                           ('../textures/hero/dark_brown/pistol/r_5.png'),
                           ('../textures/hero/dark_brown/pistol/r_6.png'),
                           ('../textures/hero/dark_brown/pistol/r_7.png'),
                           ('../textures/hero/dark_brown/pistol/r_8.png'),
                           ('../textures/hero/dark_brown/pistol/r_9.png'),
                           ('../textures/hero/dark_brown/pistol/r_10.png'),
                           ('../textures/hero/dark_brown/pistol/r_11.png')]

ANIMATION_LEFT_LASER = [('../textures/hero/dark_brown/laser/l_1.png'),
                      ('../textures/hero/dark_brown/laser/l_2.png'),
                      ('../textures/hero/dark_brown/laser/l_3.png'),
                      ('../textures/hero/dark_brown/laser/l_4.png'),
                      ('../textures/hero/dark_brown/laser/l_5.png'),
                      ('../textures/hero/dark_brown/laser/l_6.png'),
                      ('../textures/hero/dark_brown/laser/l_7.png'),
                      ('../textures/hero/dark_brown/laser/l_8.png'),
                      ('../textures/hero/dark_brown/laser/l_9.png'),
                      ('../textures/hero/dark_brown/laser/l_10.png'),
                      ('../textures/hero/dark_brown/laser/l_11.png')]

ANIMATION_LEFT_GUN = [('../textures/hero/dark_brown/gun/l_1.png'),
                      ('../textures/hero/dark_brown/gun/l_2.png'),
                      ('../textures/hero/dark_brown/gun/l_3.png'),
                      ('../textures/hero/dark_brown/gun/l_4.png'),
                      ('../textures/hero/dark_brown/gun/l_5.png'),
                      ('../textures/hero/dark_brown/gun/l_6.png'),
                      ('../textures/hero/dark_brown/gun/l_7.png'),
                      ('../textures/hero/dark_brown/gun/l_8.png'),
                      ('../textures/hero/dark_brown/gun/l_9.png'),
                      ('../textures/hero/dark_brown/gun/l_10.png'),
                      ('../textures/hero/dark_brown/gun/l_11.png')]

ANIMATION_LEFT_PISTOL = [('../textures/hero/dark_brown/pistol/l_1.png'),
                          ('../textures/hero/dark_brown/pistol/l_2.png'),
                          ('../textures/hero/dark_brown/pistol/l_3.png'),
                          ('../textures/hero/dark_brown/pistol/l_4.png'),
                          ('../textures/hero/dark_brown/pistol/l_5.png'),
                          ('../textures/hero/dark_brown/pistol/l_6.png'),
                          ('../textures/hero/dark_brown/pistol/l_7.png'),
                          ('../textures/hero/dark_brown/pistol/l_8.png'),
                          ('../textures/hero/dark_brown/pistol/l_9.png'),
                          ('../textures/hero/dark_brown/pistol/l_10.png'),
                          ('../textures/hero/dark_brown/pistol/l_11.png')]

ANIMATION_DOWN_LEFT_LASER = [('../textures/hero/dark_brown/laser/dl_1.png'),
                             ('../textures/hero/dark_brown/laser/dl_1.png')]

ANIMATION_DOWN_RIGHT_LASER = [('../textures/hero/dark_brown/laser/dr_1.png'),
                              ('../textures/hero/dark_brown/laser/dr_1.png')]

ANIMATION_DOWN_LEFT_GUN = [('../textures/hero/dark_brown/gun/dl_1.png'),
                           ('../textures/hero/dark_brown/gun/dl_1.png')]

ANIMATION_DOWN_RIGHT_GUN = [('../textures/hero/dark_brown/gun/dr_1.png'),
                            ('../textures/hero/dark_brown/gun/dr_1.png')]

ANIMATION_DOWN_LEFT_PISTOL = [('../textures/hero/dark_brown/pistol/dl_1.png'),
                             ('../textures/hero/dark_brown/pistol/dl_1.png')]

ANIMATION_DOWN_RIGHT_PISTOL = [('../textures/hero/dark_brown/pistol/dr_1.png'),
                               ('../textures/hero/dark_brown/pistol/dr_1.png')]

ANIMATION_JUMP_LEFT_LASER = [('../textures/hero/dark_brown/laser/jl_1.png', 0.1)]
ANIMATION_JUMP_RIGHT_LASER = [('../textures/hero/dark_brown/laser/jr_1.png', 0.1)]

ANIMATION_JUMP_LEFT_GUN = [('../textures/hero/dark_brown/gun/jl_1.png', 0.1)]
ANIMATION_JUMP_RIGHT_GUN = [('../textures/hero/dark_brown/gun/jr_1.png', 0.1)]

ANIMATION_JUMP_LEFT_PISTOL = [('../textures/hero/dark_brown/pistol/jl_1.png', 0.1)]
ANIMATION_JUMP_RIGHT_PISTOL = [('../textures/hero/dark_brown/pistol/jr_1.png', 0.1)]

ANIMATION_STAY_R_LASER = [('../textures/hero/dark_brown/laser/sr_1.png', 0.1)]
ANIMATION_STAY_L_LASER = [('../textures/hero/dark_brown/laser/sl_1.png', 0.1)]
ANIMATION_STAY_R_GUN = [('../textures/hero/dark_brown/gun/sr_1.png', 0.1)]
ANIMATION_STAY_L_GUN = [('../textures/hero/dark_brown/gun/sl_1.png', 0.1)]
ANIMATION_STAY_R_PISTOL = [('../textures/hero/dark_brown/pistol/sr_1.png', 0.1)]
ANIMATION_STAY_L_PISTOL = [('../textures/hero/dark_brown/pistol/sl_1.png', 0.1)]

HERO_ANIM.append({'ANIMATION_RIGHT_LASER' : ANIMATION_RIGHT_LASER, 'ANIMATION_RIGHT_GUN' : ANIMATION_RIGHT_GUN, 
                  'ANIMATION_LEFT_LASER' : ANIMATION_LEFT_LASER, 'ANIMATION_LEFT_GUN' : ANIMATION_LEFT_GUN, 
                  'ANIMATION_DOWN_LEFT_LASER' : ANIMATION_DOWN_LEFT_LASER, 'ANIMATION_DOWN_RIGHT_LASER' : ANIMATION_DOWN_RIGHT_LASER,
                  'ANIMATION_DOWN_LEFT_GUN' : ANIMATION_DOWN_LEFT_GUN, 'ANIMATION_DOWN_RIGHT_GUN' : ANIMATION_DOWN_RIGHT_GUN, 
                  'ANIMATION_JUMP_LEFT_LASER' : ANIMATION_JUMP_LEFT_LASER, 'ANIMATION_JUMP_RIGHT_LASER' : ANIMATION_JUMP_RIGHT_LASER, 
                  'ANIMATION_JUMP_LEFT_GUN' : ANIMATION_JUMP_LEFT_GUN, 'ANIMATION_JUMP_RIGHT_GUN' : ANIMATION_JUMP_RIGHT_GUN,
                  'ANIMATION_STAY_R_LASER' : ANIMATION_STAY_R_LASER, 'ANIMATION_STAY_L_LASER' : ANIMATION_STAY_L_LASER, 
                  'ANIMATION_STAY_R_GUN' : ANIMATION_STAY_R_GUN, 'ANIMATION_STAY_L_GUN' : ANIMATION_STAY_L_GUN,
                  'ANIMATION_RIGHT_PISTOL' : ANIMATION_RIGHT_PISTOL, 'ANIMATION_LEFT_PISTOL' : ANIMATION_LEFT_PISTOL,
                  'ANIMATION_DOWN_LEFT_PISTOL' : ANIMATION_DOWN_LEFT_PISTOL, 'ANIMATION_DOWN_RIGHT_PISTOL' : ANIMATION_DOWN_RIGHT_PISTOL,
                  'ANIMATION_JUMP_LEFT_PISTOL' : ANIMATION_JUMP_LEFT_PISTOL, 'ANIMATION_JUMP_RIGHT_PISTOL' : ANIMATION_JUMP_RIGHT_PISTOL,
                  'ANIMATION_STAY_R_PISTOL' : ANIMATION_STAY_R_PISTOL, 'ANIMATION_STAY_L_PISTOL' : ANIMATION_STAY_L_PISTOL})
