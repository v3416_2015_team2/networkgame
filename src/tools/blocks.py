#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pygame import *
import pyganim
from config import *
from time import time
 
class Platform(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.image = Surface((PLATFORM_WIDTH, PLATFORM_HEIGHT))
        self.image.fill(Color(PLATFORM_COLOR))
        self.image = image.load(PLATFORM_PIC)
        self.image.set_colorkey(Color(PLATFORM_COLOR))
        self.rect = Rect(x, y, PLATFORM_WIDTH, PLATFORM_HEIGHT)
        
class BlockDie(Platform):
    def __init__(self, x, y):
        Platform.__init__(self, x, y)
        self.image = image.load(DIE_BLOCK_PIC)

class Tablet(Platform):
    def __init__(self, x, y):
        Platform.__init__(self, x, y)
        self.image = image.load(TABLET_PIC)
        self.active = True
        self.spawnX = x
        self.spawnY = y
        self.lastUse = time()
        
    def update(self):
        if (time() - self.lastUse > 15):
            self.rect.x = self.spawnX
            self.rect.y = self.spawnY
            self.active = True
    
class BlockTeleport(Platform):
    def __init__(self, x, y, goX,goY):
        Platform.__init__(self, x, y)
        self.goX = goX
        self.goY = goY
        boltAnim = []
        for anim in ANIMATION_BLOCKTELEPORT:
            boltAnim.append((anim, 0.3))
        self.boltAnim = pyganim.PygAnimation(boltAnim)
        self.boltAnim.play()
        
    def update(self):
        self.image.fill(Color(PLATFORM_COLOR))
        self.boltAnim.blit(self.image, (0, 0))
        
class MovingBlock(Platform):
    def __init__(self, x, y, xvel, yvel):
        Platform.__init__(self, x, y)
        boltAnim = []
        boltAnim.append((PLATFORM_PIC, 0.3))
        self.image = image.load(PLATFORM_PIC)
        self.boltAnim = pyganim.PygAnimation(boltAnim)
        self.boltAnim.play()
        self.xvel = xvel
        self.yvel = yvel
        
    def update(self, platforms):
        self.image.fill(Color(PLATFORM_COLOR))
        self.boltAnim.blit(self.image, (0, 0))
        self.rect.y += self.yvel
        self.rect.x += self.xvel
        
        self.collide(platforms)
        
    def collide(self, platforms):
        for p in platforms:
            if sprite.collide_rect(self, p) and self != p and isinstance(p, Platform):
                self.xvel = - self.xvel
                self.yvel = - self.yvel
