#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pygame import *
import pyganim
import player
import monsters
from time import sleep, time
from config import *

class Bullets(sprite.Sprite):
    def __init__(self, startX, startY, speed, player):
        sprite.Sprite.__init__(self)
        self.image = Surface((BULLET_WIDTH, BULLET_HEIGHT))
        self.image.fill(Color(BULLET_COLOR))
        self.rect = Rect(startX, startY, BULLET_WIDTH, BULLET_HEIGHT)
        self.image.set_colorkey(Color(BULLET_COLOR))
        
        self.startX = startX
        self.startY = startY
        
        self.player = player
        
        self.xvel = speed
        
        boltAnim = []
        if (speed > 0):
            boltAnim.append((BULLET_PIC_R, 0.3))
        else:
            boltAnim.append((BULLET_PIC_L, 0.3))
        self.boltAnim = pyganim.PygAnimation(boltAnim)
        self.boltAnim.play()
         
    def update(self, platforms):
                    
        self.image.fill(Color(BULLET_COLOR))
        self.boltAnim.blit(self.image, (0, 0))
       
        self.rect.x += self.xvel
        
        self.collide(platforms)

    def collide(self, platforms):
        for p in platforms:
            if sprite.collide_rect(self, p) and self != p:
                if (isinstance(p, player.Player) or isinstance(p, Bullets)):
                    pass
                if (isinstance(p, monsters.Monster)):
                    p.hp -= 10
                    if (p.hp <= 0):
                        p.rect.x = 0
                        p.rect.y = 0
                        p.kill()
                        if (p.alive):
                            self.player.score += 50
                            p.alive = False
                self.rect.y = 0
                self.rect.x = 0                
                self.kill()

class Bullet(Bullets):
    def __init__(self, startX, startY, left, player):
        sprite.Sprite.__init__(self)
        self.image = Surface((BULLET_WIDTH, BULLET_HEIGHT))
        self.image.fill(Color(BULLET_COLOR))
        self.rect = Rect(startX, startY, BULLET_WIDTH, BULLET_HEIGHT)
        self.image.set_colorkey(Color(BULLET_COLOR))
        
        self.startX = startX
        self.startY = startY
        
        self.player = player
        
        self.xvel = left
        
        boltAnim = []
        if (left > 0):
            boltAnim.append((BULLET_PIC_R, 0.3))
        else:
            boltAnim.append((BULLET_PIC_L, 0.3))
        self.boltAnim = pyganim.PygAnimation(boltAnim)
        self.boltAnim.play()
         
    def update(self, platforms):
                    
        self.image.fill(Color(BULLET_COLOR))
        self.boltAnim.blit(self.image, (0, 0))
       
        self.rect.x += self.xvel
        
        self.collide(platforms)

    def collide(self, platforms):
        for p in platforms:
            if sprite.collide_rect(self, p) and self != p:
                if (isinstance(p, player.Player) or isinstance(p, Bullets)):
                    pass
                if (isinstance(p, monsters.Monster)):
                    p.hp -= 10
                    if (p.hp <= 0):
                        p.rect.x = 0
                        p.rect.y = 0
                        p.kill()
                        if (p.alive):
                            self.player.score += 50
                            p.alive = False
                self.rect.y = 0
                self.rect.x = 0                
                self.kill()


class Laser(Bullets):
    def __init__(self, startX, startY, screen):
        sprite.Sprite.__init__(self)
        COLOR = "#880000"
        self.image = Surface((BULLET_WIDTH, BULLET_HEIGHT))
        self.image.fill(Color(COLOR))
        self.rect = draw.line(screen, [255, 0, 0], (startX, startY), (startX + 500, startY), 5)
        self.image.set_colorkey(Color(COLOR))
        
        self.startX = startX
        self.startY = startY
         
    def update(self, platforms):
        
        self.collide(platforms)

    def collide(self, platforms):
        for p in platforms:
            if sprite.collide_rect(self, p) and self != p:
                self.rect.y = 0
                self.rect.x = 0                
                self.kill()

class KittenBullet(Bullets):
    def __init__(self, startX, startY, left, player):
        sprite.Sprite.__init__(self)
        self.image = Surface((KITTEN_WIDTH, KITTEN_HEIGHT))
        self.image.fill(Color(BULLET_COLOR))
        self.rect = Rect(startX, startY, KITTEN_WIDTH, KITTEN_HEIGHT)
        self.image.set_colorkey(Color(BULLET_COLOR))
        self.startX = startX
        self.startY = startY
        self.xvel = left
        
        self.player = player
        
        self.timeOfCollide = None
        
        boltAnim = []
        boltAnim.append((KITTEN_PIC, 0.3))
        self.boltAnim = pyganim.PygAnimation(boltAnim)
        self.boltAnim.play()
    
    def update(self, platforms):
        if (self.timeOfCollide != None and time() - self.timeOfCollide > 0.2):
            self.rect.y = 0
            self.rect.x = 0
            self.kill()
               
        self.image.fill(Color(BULLET_COLOR))
        self.boltAnim.blit(self.image, (0, 0))
       
        self.rect.x += self.xvel
        
        if (self.timeOfCollide == None):
            self.collide(platforms)
        
    def collide(self, platforms):
        for p in platforms:            
            if sprite.collide_rect(self, p) and self != p:
                if (isinstance(p, player.Player) or isinstance(p, Bullets)):
                    continue
                if (isinstance(p, monsters.Monster)):
                    p.hp -= 50
                    if (p.hp <= 0):
                        p.rect.x = 0
                        p.rect.y = 0
                        p.kill()
                        if (p.alive):
                            self.player.score += 50
                            p.alive = False
                
                boltAnim = []
                boltAnim.append((BOOM_PIC, 0.3))
                self.boltAnim = pyganim.PygAnimation(boltAnim)
                self.boltAnim.play()
                self.boltAnim.blit(self.image, (0, 0))
                
                self.timeOfCollide = time()
                self.xvel = 0
