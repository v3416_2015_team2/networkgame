import socket
import logging
import json
import pygame
import string
import sys, traceback
from time import sleep
from threading import Thread, Event
from pygame.locals import *

logger = logging.getLogger((__name__))       

class client:
    
    def __init__(self):
        self.UDP_IP = "127.0.0.1"
        self.UDP_PORT = 5005
        self.address = (self.UDP_IP, self.UDP_PORT)  
        self.clientIp = socket.gethostbyname(socket.gethostname())
        
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(1)
        
        self.connected = False
        self.userName = 'Guest'
        self.thrd = None
        self.selectedMap = 1
        self.selectedHero = 1
        self.score = 0
        self.hosting = False
        self.players = []
    
    def connect(self, serverIP):
        try:
            data = {'userName' : self.userName, 'act' : 'connect'}
            data_json = json.dumps(data)
            
            self.sock.sendto(data_json, self.address)
            
            try:
                recv_data, addr = self.sock.recvfrom(2048)
            except:
                logger.debug("Server does not response!")
                return "Server does not response!"
            
            data_arr = json.loads(recv_data)
            
            if (data_arr['answer'] == "ok"):       
                self.connected = True
            
            return data_arr['msg']
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
    
    def waitPlayers(self):
        try:
            recv_data, addr = self.sock.recvfrom(2048)
        except:
            logger.debug("No players!")
            return
        
        data_arr = json.loads(recv_data)
        
        if (data_arr['act'] == "join"):  
            self.players.append(data_arr['player'])
    
    def waitForStart(self, game):
        try:
            if (self.connected == False or game == self.userName):
                return False
            
            try:
                recv_data, addr = self.sock.recvfrom(2048)
            except:
                logger.debug("Server does not response!")
                return False
            
            data_arr = json.loads(recv_data)
            
            if (data_arr['act'] == 'startGame' and data_arr['game'] == game):
                logger.debug('Game is started: ' + game)
                return True
            else:
                logger.debug("Server does not response!")
                return False
            
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            return False
    
    def joinGame(self, game):
        try:
            if (self.connected == False):
                return False
            
            data = {'userName' : self.userName, 'act' : 'joinGame', 'gameName' : game}
            data_json = json.dumps(data)
            
            self.sock.sendto(data_json, self.address)
            
            try:
                recv_data, addr = self.sock.recvfrom(2048)
            except:
                logger.debug("Server does not response!")
                return False
            
            data_arr = json.loads(recv_data)
            
            if (data_arr['answer'] == "ok"):
                logger.debug('Game is joined: ' + game)
                return True
            else:
                logger.debug("Server does not response!")
                return False
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            return False
            
    def findGames(self):
        try:
            if (self.connected == False):
                return ''
            
            data = {'userName' : self.userName, 'act' : 'findGames'}
            data_json = json.dumps(data)
            
            self.sock.sendto(data_json, self.address)
            
            try:
                recv_data, addr = self.sock.recvfrom(2048)
            except:
                logger.debug("Server does not response!")
                return ''
            
            data_arr = json.loads(recv_data)
            
            if (data_arr['answer'] == "ok"):
                logger.debug('Message sent: ' + str(data))
                return str(data_arr['games'])
            else:
                logger.debug("Server does not response!")
                return ''
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            return ''
    
    def sendMessage(self, msg):
        try:
            if (self.connected == False):
                return
            
            data = {'userName' : self.userName}
            data.update(msg)
            data_json = json.dumps(data)
            
            self.sock.sendto(data_json, self.address)
            
            try:
                recv_data, addr = self.sock.recvfrom(2048)
                logger.debug('Message sent: ' + str(msg))
            except:
                logger.debug("No data!")
                #return False
            
            #data_arr = json.loads(recv_data)
            
            #if (data_arr['answer'] == "ok"):
            #    self.connected = True
            #else:
            #    logger.debug("Server does not response!")            
            #    self.connected = False
            #
            #return self.connected
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            #return False
        
    def ping(self):
        try:
            data = {'userName' : self.userName, 'act' : 'ping'}
            data_json = json.dumps(data)
            
            #logger.debug('ping server...')
            
            self.sock.sendto(data_json, self.address)
            
            try:
                recv_data, addr = self.sock.recvfrom(2048)
            except:
                logger.debug("Server does not response!")
                return False
            
            return True
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            return False
        
    def changeName(self, newName):
        try:
            if (self.connected == False):
                self.userName = newName
                return 'Username was changed!'            
            
            data = {'userName' : self.userName, 'act' : 'rename', 'newName' : newName}
            data_json = json.dumps(data)
            
            self.sock.sendto(data_json, self.address)
            
            try:
                recv_data, addr = self.sock.recvfrom(2048)
            except socket.timeout:
                logger.debug("Server does not response!")
                return "Server does not response!"
            
            data_arr = json.loads(recv_data)
            
            if (data_arr['answer'] == "ok"):
                self.userName = newName
            
            return data_arr['msg']
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))