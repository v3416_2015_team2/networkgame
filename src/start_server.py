#!/usr/bin/env python
# -*- coding: utf-8 -*-

import server
import logging
from time import sleep, time
from threading import Thread
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import pyqtSlot
import socket
import string

MAX_PING = 10

logger = logging.getLogger((__name__))

logger.info('Starting NetworkGame Server')
logfile = '../logs/server.log'
loglevel = logging.DEBUG

logging.basicConfig(filename = logfile, format = '%(asctime)s [%(levelname)s] %(name)s: %(message)s', level = loglevel)

server = server.server()

class pingThread(Thread):
    def __init__(self):
        Thread.__init__(self)
    def run(self):
        global server
        
        while True:
            for userName, curUser in server.users.items():
                if (float(time() - curUser.lastAct) > MAX_PING):
                    server.deleteUser(userName)
            
            sleep(5)

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

@pyqtSlot()
def changeState(server_state):
    if (server.state == 1):
        server_state.setText(_translate("MainWindow", "online", None))
        server_state.setStyleSheet(_fromUtf8("QLabel {color: green}"))
    else:
        server_state.setText(_translate("MainWindow", "offline", None))
        server_state.setStyleSheet(_fromUtf8("QLabel {color: red}"))
        
@pyqtSlot()
def updatePlayers(connected):
    connected.setText(_translate("MainWindow", str(server.connected), None))    
   

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(970, 789)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        
        self.start_server = QtGui.QPushButton(self.centralwidget)
        self.start_server.setGeometry(QtCore.QRect(350, 530, 191, 51))
        self.start_server.setObjectName(_fromUtf8("start_server"))
        
        self.stop_server = QtGui.QPushButton(self.centralwidget)
        self.stop_server.setGeometry(QtCore.QRect(350, 600, 191, 51))
        self.stop_server.setObjectName(_fromUtf8("stop_server"))
        
        self.restart_server = QtGui.QPushButton(self.centralwidget)
        self.restart_server.setGeometry(QtCore.QRect(350, 670, 191, 51))
        self.restart_server.setObjectName(_fromUtf8("restart_server"))
        
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(690, 530, 41, 17))
        self.label.setObjectName(_fromUtf8("label"))
        
        self.server_state = QtGui.QLabel(self.centralwidget)
        self.server_state.setGeometry(QtCore.QRect(740, 530, 66, 17))
        self.server_state.setAutoFillBackground(False)
        self.server_state.setStyleSheet(_fromUtf8("QLabel {color: red}"))
        self.server_state.setTextFormat(QtCore.Qt.PlainText)
        self.server_state.setObjectName(_fromUtf8("server_state"))
        
        self.connected = QtGui.QLabel(self.centralwidget)
        self.connected.setGeometry(QtCore.QRect(780, 560, 16, 17))
        self.server_state.setAutoFillBackground(False)
        self.server_state.setTextFormat(QtCore.Qt.PlainText)
        self.connected.setObjectName(_fromUtf8("connected"))
        
        self.server_ip = QtGui.QLabel(self.centralwidget)
        self.server_ip.setGeometry(QtCore.QRect(740, 590, 66, 17))
        self.server_ip.setObjectName(_fromUtf8("server_ip"))
        
        self.label_2 = QtGui.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(650, 560, 81, 20))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        
        self.label_3 = QtGui.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(660, 590, 81, 20))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        
        self.logo = QtGui.QLabel(self.centralwidget)
        self.logo.setGeometry(QtCore.QRect(130, 0, 691, 511))
        self.logo.setText(_fromUtf8(""))
        self.logo.setPixmap(QtGui.QPixmap(_fromUtf8("../textures/menu/pythonlogo.jpg")))
        self.logo.setObjectName(_fromUtf8("logo"))
        
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 970, 25))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        
        self.start_server.clicked.connect(lambda: server.start())
        self.stop_server.clicked.connect(lambda: server.stop())
        self.restart_server.clicked.connect(lambda: server.restart())
        
        server.stateSignal.connect(lambda: changeState(self.server_state))
        server.connectSignal.connect(lambda: updatePlayers(self.connected))

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)    

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.start_server.setText(_translate("MainWindow", "start server", None))
        self.stop_server.setText(_translate("MainWindow", "stop server", None))
        self.restart_server.setText(_translate("MainWindow", "restart server", None))
        self.label.setText(_translate("MainWindow", "state: ", None))
        self.server_state.setText(_translate("MainWindow", "offline", None))
        self.connected.setText(_translate("MainWindow", "0", None))
        self.server_ip.setText(_translate("MainWindow", socket.gethostbyname(socket.gethostname()), None))
        self.label_2.setText(_translate("MainWindow", "connected:", None))
        self.label_3.setText(_translate("MainWindow", "server ip:", None))
        

if __name__ == "__main__":
    import sys
    
    thrd = pingThread()
    thrd.setDaemon(True)
    thrd.start()
    
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
    
