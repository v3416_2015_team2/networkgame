#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import logging
import json
import sys, traceback
import user
from time import time
from threading import Thread, Event
from PyQt4 import QtCore

logger = logging.getLogger((__name__))

def serverThread(serverObj, stop_event):
    try:
        
        while (not stop_event.is_set()):
            
            recv_data, addr = serverObj.server.recvfrom(1024)
            
            logger.debug("received message: " + recv_data)
            
            data_arr = json.loads(recv_data)
                                    
            if (data_arr['act'] == "connect"):
                if (serverObj.addUser(data_arr['userName'], addr)):
                    data = {'act' : 'connect', 'answer' : 'ok', 'msg' : 'Client was successfully connected to server.'}
                    data_json = json.dumps(data)
                else:
                    data = {'act' : 'connect', 'answer' : 'error', 'msg' : 'Username is already in use!'}
                    data_json = json.dumps(data)
                    
                serverObj.server.sendto(data_json, addr)
                
            if (data_arr['act'] == "newGameT"):
                if (serverObj.users.has_key(data_arr['userName'])):
                    serverObj.users[data_arr['userName']].gameCreated = True
                    
            if (data_arr['act'] == "newGameF"):
                if (serverObj.users.has_key(data_arr['userName'])):
                    serverObj.users[data_arr['userName']].gameCreated = False
                    
            if (data_arr['act'] == "findGames"):
                if (serverObj.users.has_key(data_arr['userName'])):
                    data = {'act' : 'findGames', 'answer' : 'ok'}
                    games = ''
                    for user in serverObj.users:
                        if (serverObj.users[user].gameCreated == True):
                            games += user + ","
                            
                    if (len(games) > 0):
                        games = games[0:-1]
                        
                    data.update({'games' : str(games)})
                    data_json = json.dumps(data)
                    serverObj.server.sendto(data_json, addr)
                    
            if (data_arr['act'] == "joinGame"):
                if (serverObj.users.has_key(data_arr['userName'])):
                    if (serverObj.users.has_key(data_arr['gameName']) and serverObj.users[data_arr['gameName']].gameCreated == True):
                        data = {'act' : 'join', 'player' : data_arr['userName']}
                        data_json = json.dumps(data)
                        serverObj.server.sendto(data_json, serverObj.users[data_arr['gameName']].addr)
                        data = {'act' : 'joinGame', 'answer' : 'ok'}
                        data_json = json.dumps(data)
                        serverObj.server.sendto(data_json, addr)
                        
            if (data_arr['act'] == "startGame"):
                if (serverObj.users.has_key(data_arr['userName'])):
                    data = {'act' : 'startGame', 'game' : data_arr['userName']}
                    data_json = json.dumps(data)
                    for user in serverObj.users:
                        serverObj.server.sendto(data_json, serverObj.users[user].addr)
                
            if (data_arr['act'] == "disconnect"):
                if (serverObj.addUser(data_arr['userName'])):
                    data = {'act' : 'disconnect', 'answer' : 'ok', 'msg' : 'Client was disconnected to server.'}
                    data_json = json.dumps(data)
                    serverObj.deleteUser(data_arr['userName'])                    
                    serverObj.server.sendto(data_json, addr)
                    
            elif (data_arr['act'] == "keydown" or data_arr['act'] == "keyup"):
                if (serverObj.users.has_key(data_arr['userName'])):
                    data = {'act' : data_arr['act'], 'answer' : 'ok'}
                    data_json = json.dumps(data)
                    serverObj.server.sendto(data_json, addr)
                    
            elif (data_arr['act'] == "rename"):
                if (serverObj.users.has_key(data_arr['userName'])):
                    if (serverObj.users.has_key(data_arr['newName'])):
                        data = {'act' : 'ping', 'answer' : 'error', 'msg' : 'Username is already in use!'}
                    else:
                        data = {'act' : 'ping', 'answer' : 'ok', 'msg' : 'Username was saved!'}
                        serverObj.renameUser(data_arr['userName'], data_arr['newName'])
                        
                    data_json = json.dumps(data)
                    serverObj.server.sendto(data_json, addr)
                    
            elif (data_arr['act'] == "ping"):
                if (serverObj.users.has_key(data_arr['userName'])):
                    data = {'act' : 'ping', 'answer' : 'ok'}
                    data_json = json.dumps(data)
                    serverObj.server.sendto(data_json, addr)
                    serverObj.users[data_arr['userName']].lastAct = time()
            
    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
        
class server(QtCore.QThread):
    stateSignal = QtCore.pyqtSignal()
    connectSignal = QtCore.pyqtSignal()
    
    def __init__(self, port = 5005):        
        super(server, self).__init__()
        
        self.port = port
        self.conn = None
        self.thrd = None
        self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind(('', self.port))
        self.state = 0 # 0 - stopped, 1 - running
        self.connected = 0
        self.users = {}
            
    def start(self):
        try:
            if (self.state == 0):
                logger.debug('Starting server...')
                
                self.threadStop = Event()
                self.thrd = Thread(target = serverThread, args = (self, self.threadStop))
                self.thrd.setDaemon(True)
                self.thrd.start()
                
                self.state = 1
                self.stateSignal.emit()
                
                logger.debug('Server was started successfully!')
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            
    def stop(self):
        try:
            if (self.state == 1):
                logger.debug('Stopping server...')
                
                self.threadStop.set()
                self.users.clear()
                self.connected = 0
                self.connectSignal.emit()
                
                self.state = 0
                self.stateSignal.emit()
                
                logger.debug('Server was stopped!')
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
        
    def restart(self):
        self.stop()
        self.start()
        
    def addUser(self, userName, addr):
        try:
            if (self.users.get(userName)):
                return False
            
            newUser = user.user(userName, time(), addr)
            if (userName == 'test'):
                newUser.gameCreated = True
            self.users[userName] = newUser
            self.connected += 1
            self.connectSignal.emit()
            logger.debug("User connected: " + userName)
            return True
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            return False
    
    def deleteUser(self, userName):
        try:
            self.connected -= 1
            del self.users[userName]
            logger.debug("User disconnected: " + userName)
            self.connectSignal.emit()
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))
    
    def renameUser(self, oldName, newName):
        try:
            del self.users[oldName]
            newUser = user.user(newName, time())   
            self.users[newName] = newUser     
            logger.debug("User was renamed (%s -> %s)" % (oldName, newName))
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error("Got exception " + repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))